$(document).ready(function(){
	$('#text-field').keypress(function(e){
		$('html, body').animate({scrollTop: $(document).height()}, 50);

		// CHAPTER 4 [start]
		if (e.which == 13 && text_count == 4000){
			$('#current_chapter').empty();
			$('#current_chapter').append(chapter("Kapitel 4: Es liegt Maggi in der Luft"));
			
			$('#game').append(storyText(
				name_norbert,
				"Ihr betretet einen Raum der anscheinend im Freien liegt, da die Vögel im Winde säuseln, die Magie durch die Luft zurrt und Fuchs und Hase sic ......"
			));
			$('#game').append(br);

			text_count = 4001;
			return;
		}

		if (e.which == 13 && text_count == 4001){
			$('#game').append(storyText(
				name_freia,
				"Guck mal. Ich hab die Überschrift da oben mit Maggi gezaubert ^^"
			));
			$('#game').append(br);

			text_count = 4002;
			return;
		}

		if (e.which == 13 && text_count == 4002){
			$('#game').append(storyText(
				name_norbert,
				"Ernsthaft O.O"
			));
			$('#game').append(br);

			text_count = 4003;
			return;
		}

		if (e.which == 13 && text_count == 4003){
			$('#game').append(storyText(
				name_norbert,
				"Didi, bitte sag mir das die als Kind 5 mal in die Luft und nur 1 mal aufgefangen wurde Ö.O"
			));
			$('#game').append(br);

			text_count = 4004;
			return;
		}

		if (e.which == 13 && text_count == 4004){
			$('#game').append(storyText(
				name_freia,
				"Wieso denn das? Ich hatte eigentlich eine ganz normale Kindheit. Ich hab mit meinem Vater immer verstecken gespielt. Ganze 17 Jahre hab ich gewonnen. Bis mich die Polizei in meinem Versteck gefunden und zur Darkmouth Universität geschickt hat."
			));
			$('#game').append(br);

			text_count = 4005;
			return;
		}

		if (e.which == 13 && text_count == 4005){
			$('#game').append(storyText(
				name_norbert,
				"Jaaaaaa. Weil du ja bekanntlicherweise so KLUG BIST."
			));
			$('#game').append(br);

			text_count = 4006;
			return;
		}

		if (e.which == 13 && text_count == 4006){
			$('#game').append(storyText(
				name_freia,
				"Jeap. Und deswegen verstehe ich auch deine Aussage nicht, dass meine intellektuelle Fähigkeiten nicht ausreichen, um den gegebenen Sachverhalt vollständig zu erfassen. Denn du solltest ja wissen: Populanten von transparenten Domizilen sollten mit fester Materie keine transzendenten Bewegungen durchführen."
			));
			$('#game').append(br);

			text_count = 4007;
			return;
		}

		if (e.which == 13 && text_count == 4007){
			$('#game').append(storyText(
				name_norbert,
				"Noch einen Satz bitte."
			));
			$('#game').append(br);

			text_count = 4008;
			return;
		}

		if (e.which == 13 && text_count == 4008){
			$('#game').append(storyText(
				name_freia,
				"Es existiert ein Interesse an der generellen Rezession der Applikation relativ primitiver Methoden komplementär zur Favorisierung adäquater komplexer Algorithmen."
			));
			$('#game').append(br);

			text_count = 4009;
			return;
		}

		if (e.which == 13 && text_count == 4009){
			$('#game').append(storyText(
				name_norbert,
				"Okay Loana. Du kannst rauskommen. Sehr witzig Freias Körper zu übernehmen, aber die \"leichte\" Intellektuelle Steigerung ihres Wortschatzes hat dich verraten *Lach*"
			));
			$('#game').append(br);

			text_count = 4010;
			return;
		}

		if (e.which == 13 && text_count == 4010){
			$('#game').append(storyText(
				name_waechterin_loana,
				"Ach verdammt Norbert. Du schaffst es doch immer wieder mich zu durchschauen *Seufz*"
			));
			$('#game').append(br);

			text_count = 4011;
			return;
		}

		if (e.which == 13 && text_count == 4011){
			$('#game').append(storyText(
				name_norbert,
				"Wenn du wie ich 21 Jahre mit Freia in einem Kopf leben würdest, dann hättest du es auch bemerkt. Glaub mir O.o"
			));
			$('#game').append(br);

			text_count = 4012;
			return;
		}

		if (e.which == 13 && text_count == 4012){
			$('#game').append(storyText(
				name_freia,
				"Mein Bruder ist ein Einzelkind ^^"
			));
			$('#game').append(br);

			text_count = 4013;
			return;
		}

		if (e.which == 13 && text_count == 4013){
			$('#game').append(storyText(
				name_waechterin_loana,
				"Aber mein liebes Kind. Das ist .... Komplett unmöglich. Überleg doch mal. Wenn du einen Bruder hast, wie kann er dann ein Einzelkind sein? Ö.O"
			));
			$('#game').append(br);

			text_count = 4014;
			return;
		}

		if (e.which == 13 && text_count == 4014){
			$('#game').append(storyText(
				name_norbert,
				"Vergiss es, die Logik ist bei ihr schon lange ausgeschaltet. Sie lebt in ihrer eigenen kleinen Welt *Seufz*"
			));
			$('#game').append(br);

			text_count = 4015;
			return;
		}

		if (e.which == 13 && text_count == 4015){
			$('#game').append(storyText(
				name_freia,
				"Sieh mal Wilhelm, da liegt ein aufgegessener Keks ^^"
			));
			$('#game').append(br);

			text_count = 4016;
			return;
		}

		if (e.which == 13 && text_count == 4016){
			$('#game').append(storyText(
				name_norbert,
				"Siehst du Loana?"
			));
			$('#game').append(br);

			text_count = 4017;
			return;
		}

		if (e.which == 13 && text_count == 4017){
			$('#game').append(storyText(
				name_waechterin_loana,
				"Ach du lieber Gott. Kann man dem Kind denn nicht helfen?"
			));
			$('#game').append(br);

			text_count = 4018;
			return;
		}

		if (e.which == 13 && text_count == 4018){
			$('#game').append(storyText(
				name_norbert,
				"Nope. Höchstens das Game hier beenden und dann ab nach Hause *Seufz*"
			));
			$('#game').append(br);

			text_count = 4019;
			return;
		}

		if (e.which == 13 && text_count == 4019){
			$('#game').append(storyText(
				name_waechterin_loana,
				"Also gut. Aber ihr müsst das Rätsel so wie jeder andere Eindringling lösen. Da solltest du aber wissen, dass ich dort keine Ausnahmen mache *Lach*"
			));
			$('#game').append(br);

			text_count = 4020;
			return;
		}

		if (e.which == 13 && text_count == 4020){
			$('#game').append(storyText(
				name_norbert,
				"Oh das Rätsel werden wir schon lösen, da mach dir mal keine Sorgen :D"
			));
			$('#game').append(br);

			text_count = 4021;
			return;
		}

		if (e.which == 13 && text_count == 4021){
			$('#game').append(storyText(
				name_waechterin_loana,
				"Also gut. Dann wollen wir mal."
			));
			$('#game').append(br);

			text_count = 4022;
			return;
		}

		if (e.which == 13 && text_count == 4022){
			$('#game').append(storyOnlyText(
				"*Räusper*"
			));
			$('#game').append(br);

			text_count = 4023;
			return;
		}

		if (e.which == 13 && text_count == 4023){
			$('#game').append(storyText(
				name_waechterin_loana,
				"Ich bin Loana die Heilige Wächterin des Springbrun ...."
			));
			$('#game').append(br);

			text_count = 4024;
			return;
		}

		if (e.which == 13 && text_count == 4024){
			$('#game').append(storyText(
				name_freia,
				"Hi. Ich bin Freia und das ist Norbert. Wir sind gerade nur auf der Durchreise und wollen dich nicht weiter stören."
			));
			$('#game').append(br);

			text_count = 4025;
			return;
		}

		if (e.which == 13 && text_count == 4025){
			$('#game').append(storyText(
				name_waechterin_loana,
				"Ja ... Ähmmm. Dankeschön. *Nachdenk* Ach ja. Ich bin Loana die Heilige Wächterin des HEILIGEN SPRINGBRUNNEN VON TSRUDTZTEJBAHHCI und ich bin hier um eure Fantasie, euren Mut und eure ....."
			));
			$('#game').append(br);

			text_count = 4026;
			return;
		}

		if (e.which == 13 && text_count == 4026){
			$('#game').append(storyOnlyText(
				"*Freia holt einen Popel aus der Nase und riecht dran*"
			));
			$('#game').append(br);

			text_count = 4027;
			return;
		}

		if (e.which == 13 && text_count == 4027){
			$('#game').append(storyText(
				name_waechterin_loana,
				"Ähmmm. *Sie spricht langsamer* Intelligenz ..... ಠ_ಠ"
			));
			$('#game').append(br);

			text_count = 4028;
			return;
		}

		if (e.which == 13 && text_count == 4028){
			$('#game').append(storyOnlyText(
				"*Freia redet währenddessen mit ihrem Spiegelbild im Springbrunnen*"
			));
			$('#game').append(br);

			text_count = 4029;
			return;
		}

		if (e.which == 13 && text_count == 4029){
			$('#game').append(storyText(
				name_freia,
				"Hi. Ich bin Freia und wie heißt du?"
			));
			$('#game').append(br);

			text_count = 4030;
			return;
		}

		if (e.which == 13 && text_count == 4030){
			$('#game').append(storyText(
				name_springbrunnen,
				"........."
			));
			$('#game').append(br);

			text_count = 4031;
			return;
		}

		if (e.which == 13 && text_count == 4031){
			$('#game').append(storyText(
				name_freia,
				"Echt, das ist ja echt super Cool. Erzähl doch mal nen Witz ^^"
			));
			$('#game').append(br);

			text_count = 4032;
			return;
		}

		if (e.which == 13 && text_count == 4032){
			$('#game').append(storyText(
				name_springbrunnen,
				"........."
			));
			$('#game').append(br);

			text_count = 4033;
			return;
		}

		if (e.which == 13 && text_count == 4033){
			$('#game').append(storyText(
				name_freia,
				"Hahahahahahahahahha. Ne den kannte ich noch nicht. Und was hat das (°oo°) dann zum <(\") gesagt?"
			));
			$('#game').append(br);

			text_count = 4034;
			return;
		}

		if (e.which == 13 && text_count == 4034){
			$('#game').append(storyText(
				name_springbrunnen,
				"........."
			));
			$('#game').append(br);

			text_count = 4035;
			return;
		}

		if (e.which == 13 && text_count == 4035){
			$('#game').append(storyText(
				name_freia,
				"Was wirklich? Und dann Michelle das Kleid von Jenny einfach so angezogen, ohne vorher Maxine oder Celina zu fragen?"
			));
			$('#game').append(br);

			text_count = 4036;
			return;
		}

		if (e.which == 13 && text_count == 4036){
			$('#game').append(storyText(
				name_waechterin_loana,
				"Okay okay okay. Bleiben wir bei Fantasie und Mut (눈_눈)"
			));
			$('#game').append(br);

			text_count = 4037;
			return;
		}

		if (e.which == 13 && text_count == 4037){
			$('#game').append(storyText(
				name_norbert,
				"Gute Idee O_O"
			));
			$('#game').append(br);

			text_count = 4038;
			return;
		}

		if (e.which == 13 && text_count == 4038){
			$('#game').append(storyText(
				name_waechterin_loana,
				"Erde an Freia!!!!"
			));
			$('#game').append(br);

			text_count = 4039;
			return;
		}

		if (e.which == 13 && text_count == 4039){
			$('#game').append(storyText(
				name_freia,
				"Hmm?"
			));
			$('#game').append(br);

			text_count = 4040;
			return;
		}

		if (e.which == 13 && text_count == 4040){
			$('#game').append(storyText(
				name_waechterin_loana,
				"Hast du Überhaupt ein Stückchen aufgepasst? -.-"
			));
			$('#game').append(br);

			text_count = 4041;
			return;
		}

		if (e.which == 13 && text_count == 4041){
			$('#game').append(storyText(
				name_waechterin_loana,
				"Siehst du überhaupt den Ernst der Lage? Wenn das Rätsel hier nicht gelöst wird, könnt ihr beide nicht weiterkommen."
			));
			$('#game').append(br);

			text_count = 4042;
			return;
		}

		if (e.which == 13 && text_count == 4042){
			$('#game').append(storyText(
				name_freia,
				"Aber ich muss doch zu meinen Chamäleon *Schnüff*"
			));
			$('#game').append(br);

			text_count = 4043;
			return;
		}

		if (e.which == 13 && text_count == 4043){
			$('#game').append(storyText(
				name_waechterin_loana,
				"Sie hat ein Chamäleon? O.o"
			));
			$('#game').append(br);

			text_count = 4044;
			return;
		}

		if (e.which == 13 && text_count == 4044){
			$('#game').append(storyText(
				name_norbert,
				"Jeap und das ist ihr echt wichtig. Versuch die Frage einfach in Ganz Simpel und ohne viel Mittelalterliches Wirrwarr gerede zu formulieren. Bitte. Sonst sind die beiden hier noch 5 Jahre gefangen `.` Ich weiß du machst daraus immer gerne viel Mystik und Epic Zeug rein, *Blablabla, aber bitte mach das doch bei den Nächsten und nicht unbedingt bei ihr *Seufz*"
			));
			$('#game').append(br);

			text_count = 4045;
			return;
		}

		if (e.which == 13 && text_count == 4045){
			$('#game').append(storyText(
				name_waechterin_loana,
				"Okay okay okay. Bevor das Mädchen hier noch einen kompletten Heulkrampf bekommt."
			));
			$('#game').append(br);

			text_count = 4046;
			return;
		}

		if (e.which == 13 && text_count == 4046){
			$('#game').append(storyText(
				name_waechterin_loana,
				"Also Freia. Ganz einfach. Auf dem Grund des HEILIGEN SPRINGBRUNNEN VON TSRUDTZTEJBAHHCI liegt ein Heiliger Talisman der Ididsoahc. Deine Aufgabe ist es an dieses heranzukommen. Du kannst alles benutzen was du hier findest."
			));
			$('#game').append(br);

			text_count = 4047;
			return;
		}

		if (e.which == 13 && text_count == 4047){
			$('#game').empty();
			$('#game').append(storyOnlyText(
				"Also, was tust du?",
				" ",
				"1 - Spezialfähigkeit 1 benutzen (Magie)",
				"2 - Spezialfähigkeit 2 benutzen (Magie)",
				"3 - Spezialfähigkeit 3 benutzen (Magie)"
			));
			$('#game').append(br);

			text_count = 4048;
			return;
		}

		if (e.which == 13 && text_count == 4048){
			$('#game').append(storyText(
				name_waechterin_loana,
				"Wow wow wow. Was ist denn da los? O.o"
			));
			$('#game').append(br);

			text_count = 4049;
			return;
		}

		if (e.which == 13 && text_count == 4049){
			$('#game').append(storyText(
				name_freia,
				"Och die alten Auswahlmöglichkeiten fand ich Dof. Darum hab ich die kurz ausgetauscht ^^"
			));
			$('#game').append(br);

			text_count = 4050;
			return;
		}

		if (e.which == 13 && text_count == 4050){
			$('#game').append(storyText(
				name_waechterin_loana,
				"Dof? Aber das heißt doch ....."
			));
			$('#game').append(br);

			text_count = 4051;
			return;
		}

		if (e.which == 13 && text_count == 4051){
			$('#game').append(storyText(
				name_norbert,
				"Vergiss es meine gute, das hab ich schon LANGE aufgegeben *Seufz*"
			));
			$('#game').append(br);

			text_count = 4052;
			return;
		}

		if (e.which == 13 && text_count == 4052){
			$('#game').append(storyText(
				name_waechterin_loana,
				"Ach Wirklich?. Also du kannst jedenfalls nicht einfach die Auswahlmöglichkeiten ändern. Das ist gegen die Regeln!!! Wofür hab ich mir bitte Jahrelang den Kopf zerbrochen, um ein schönes und anspruchsvolles Rätsel zu gestalten? O.o"
			));
			$('#game').append(br);

			text_count = 4053;
			return;
		}

		if (e.which == 13 && text_count == 4053){
			$('#game').append(storyText(
				name_freia,
				"Aber das Rätsel war Dof *Mähhh*"
			));
			$('#game').append(br);

			text_count = 4054;
			return;
		}

		if (e.which == 13 && text_count == 4054){
			$('#game').append(storyText(
				name_waechterin_loana,
				"Gut. Dann versuch das Rätsel mal mit Magie zu lösen -.-"
			));
			$('#game').append(br);

			text_count = 4055;
			return;
		}

		if (e.which == 13 && text_count == 4055){
			$('#game').append(storyText(
				name_waechterin_loana,
				"Viel Glück dabei. o.o"
			));
			$('#game').append(br);

			text_count = 4056;
			return;
		}

		if (e.which == 13 && text_count == 4056){
			$('#game').append(storyText(
				name_freia,
				"Also mein Freund. Du musst etwas auswählen ^^. Und wenn es mit Magie zu tun haben sollte ..."
			));
			$('#game').append(br);

			text_count = 4057;
			return;
		}

		if (e.which == 13 && text_count == 4057){
			$('#game').append(storyText(
				name_waechterin_loana,
				"ES BLEIBT NICHTS ANDERES ÜBRIG!!!!!!"
			));
			$('#game').append(br);

			text_count = 4058;
			return;
		}

		if (e.which == 13 && text_count == 4058){
			$('#game').append(storyText(
				name_norbert,
				"Pssssssst. Was war denn mit Ruhe und Besonnenheit? ;)"
			));
			$('#game').append(br);

			text_count = 4059;
			return;
		}

		if (e.which == 13 && text_count == 4059){
			$('#game').append(storyText(
				name_waechterin_loana,
				"Treib es nicht zu weit mein Lieber *Fauch*"
			));
			$('#game').append(br);

			text_count = 4060;
			return;
		}

		if (e.which == 13 && text_count == 4060){
			$('#game').empty();
			$('#game').append(storyText(
				name_freia,
				"Lass dich nicht beirren. Also was wählst du? ^^",
				" ",
				"1 - Spezialfähigkeit 1 benutzen (Magie)",
				"2 - Spezialfähigkeit 2 benutzen (Magie)",
				"3 - Spezialfähigkeit 3 benutzen (Magie)"
			));
			$('#game').append(br);

			text_count = 4061;
			return;
		}

		if (e.which == 13 && text_count == 4061){
			var question_4061_text = $('#text-field').val().toUpperCase();
			if(question_4061_text.length >= 1){
				if(question_4061_text == "1" 
					|| question_4061_text == "2"
					|| question_4061_text == "3"
					|| question_4061_text == "SPEZIALFÄHIGKEIT 1 BENUTZEN"
					|| question_4061_text == "SPEZIALFÄHIGKEIT 2 BENUTZEN"
					|| question_4061_text == "SPEZIALFÄHIGKEIT 3 BENUTZEN"){
					$('#game').empty();
					$('#text-field').val("");
					$('#game').append(storyText(
						name_ich,
						"Ähmmm. Freia. Trotz diverser Auswahlmöglichkeiten, hab ich mich für Magie entschieden *glaub ich ....*"
					));
					$('#game').append(br);

					text_count = 4062;
					return;
				}
			}else{
				text_count = 4060;
			}
			return;
		}

		if (e.which == 13 && text_count == 4062){
			$('#game').append(storyText(
				name_freia,
				"Also ich muss bis zum Ende des Brunnens kommen und das wars?"
			));
			$('#game').append(br);

			text_count = 4063;
			return;
		}

		if (e.which == 13 && text_count == 4063){
			$('#game').append(storyText(
				name_waechterin_loana,
				"*Leise flüster* Damals war es noch deutlich schwieriger, aber das würde dich ja komplett überfordern."
			));
			$('#game').append(br);

			text_count = 4064;
			return;
		}

		if (e.which == 13 && text_count == 4064){
			$('#game').append(storyText(
				name_freia,
				"Ich hab zwar nichts verstanden, aber wer flüstert der lügt ^^"
			));
			$('#game').append(br);

			text_count = 4065;
			return;
		}

		if (e.which == 13 && text_count == 4065){
			$('#game').append(storyText(
				name_freia,
				"Und was ist wenn ich einfach Wassermagie verwende und das Wasser im Brunnen austrockne?"
			));
			$('#game').append(br);

			text_count = 4066;
			return;
		}

		if (e.which == 13 && text_count == 4066){
			$('#game').append(storyText(
				name_waechterin_loana,
				"Neinnnnn nein nein nein nein nein nein nein. Das geht nicht weil .... ähmmm naja das ist so ..."
			));
			$('#game').append(br);

			text_count = 4067;
			return;
		}

		if (e.which == 13 && text_count == 4067){
			$('#game').append(storyOnlyText(
				"*Zu Norbert sprech*"
			));
			$('#game').append(br);

			text_count = 4068;
			return;
		}

		if (e.which == 13 && text_count == 4068){
			$('#game').append(storyText(
				name_waechterin_loana,
				"Kannst du mir nicht aus der Patsche helfen? Ich komm mit diesem Mädel einfach nicht klar -.-"
			));
			$('#game').append(br);

			text_count = 4069;
			return;
		}

		if (e.which == 13 && text_count == 4069){
			$('#game').append(storyText(
				name_norbert,
				"Ja Freia das ist erlaubt."
			));
			$('#game').append(br);

			text_count = 4070;
			return;
		}

		if (e.which == 13 && text_count == 4070){
			$('#game').append(storyText(
				name_waechterin_loana,
				"WIE BITTE?!?!?!?"
			));
			$('#game').append(br);

			text_count = 4071;
			return;
		}

		if (e.which == 13 && text_count == 4071){
			$('#game').append(storyText(
				name_waechterin_loana,
				"Und das nennst du aus der Patsc..."
			));
			$('#game').append(br);

			text_count = 4072;
			return;
		}

		if (e.which == 13 && text_count == 4072){
			$('#game').append(storyText(
				name_freia,
				"Eiris sazun idisi, sazun hera duoder.",
				"Suma hapt heptidun, suma heri lezidun ...."
			));
			$('#game').append(br);

			text_count = 4073;
			return;
		}

		if (e.which == 13 && text_count == 4073){
			$('#game').append(storyText(
				name_waechterin_loana,
				"Woher kennt Sie solch mächtige Magie?!?! Das benötigt Jahrhunderte Jahre lange Erfahrung, Medidation und vor allem mächtiges Magierblut!!!"
			));
			$('#game').append(br);

			text_count = 4074;
			return;
		}

		if (e.which == 13 && text_count == 4074){
			$('#game').append(storyText(
				name_freia,
				"Suma clubodun umbi cuoniouuidi:",
				"Insprinc haptbandun, inuar uigandun!!!!!!!!!!!"
			));
			$('#game').append(br);

			text_count = 4075;
			return;
		}

		if (e.which == 13 && text_count == 4075){
			$('#game').append(storyText(
				name_norbert,
				"Und als das Wasser aus dem HEILIGEN SPRINGBRUNNEN VON TSRUDTZTEJBAHHCII floss, bildete sich dort stattdessen eine Wackelpudding Masse."
			));
			$('#game').append(br);

			text_count = 4076;
			return;
		}

		if (e.which == 13 && text_count == 4076){
			$('#game').append(storyText(
				name_waechterin_loana,
				"Bitte was?!?! Aber ... Ähmm wie ... also .."
			));
			$('#game').append(br);

			text_count = 4077;
			return;
		}

		if (e.which == 13 && text_count == 4077){
			$('#game').append(storyText(
				name_norbert,
				"Nur zur Info. Unter anderem bin ich der Erzähler. Also sollte das Freia erstmal abhalten *Hah*"
			));
			$('#game').append(br);

			text_count = 4078;
			return;
		}

		if (e.which == 13 && text_count == 4078){
			$('#game').append(storyText(
				name_waechterin_loana,
				"Womit ist der Pudding denn gefüllt? Was ist wenn Sie ihn einfach aufisst???!!!"
			));
			$('#game').append(br);

			text_count = 4079;
			return;
		}

		if (e.which == 13 && text_count == 4079){
			$('#game').append(storyText(
				name_freia,
				"Oh was ist das denn? O.O"
			));
			$('#game').append(br);

			text_count = 4080;
			return;
		}

		if (e.which == 13 && text_count == 4080){
			$('#game').append(storyText(
				name_norbert,
				"Mit Wurst. Und da du bekanntlicherweise seit 21 Jahren Veganerin bist, wirst du ihn wohl nicht anrühren. Du wirst das Rätsel wohl neu Starten müssen :)"
			));
			$('#game').append(br);

			text_count = 4081;
			return;
		}

		if (e.which == 13 && text_count == 4081){
			$('#game').append(storyOnlyText(
				"*Freia holt ihr Telefon aus der ihrer Tasche und wählt eine Nummer*"
			));
			$('#game').append(br);

			text_count = 4082;
			return;
		}

		if (e.which == 13 && text_count == 4082){
			$('#game').append(storyText(
				name_waechterin_loana,
				"Wen will Sie bitte erreichen? Wir sind knapp 200000 Meter unter der Erde. Als wenn sie hier überhaupt Empf ...."
			));
			$('#game').append(br);

			text_count = 4083;
			return;
		}

		if (e.which == 13 && text_count == 4083){
			$('#game').append(storyText(
				name_norbert,
				"Meinst du wirklich, dass diese Logik in Freias kleiner Welt überhaupt existiert?"
			));
			$('#game').append(br);

			text_count = 4084;
			return;
		}

		if (e.which == 13 && text_count == 4084){
			$('#game').append(storyText(
				name_waechterin_loana,
				"Aber das ist doch unfug!!!"
			));
			$('#game').append(br);

			text_count = 4085;
			return;
		}

		if (e.which == 13 && text_count == 4085){
			$('#game').append(storyText(
				name_freia,
				"Du machst dich also jetzt auf den Weg? Supi, dann müsstest du ja ....."
			));
			$('#game').append(br);

			text_count = 4086;
			return;
		}

		if (e.which == 13 && text_count == 4086){
			$('#game').append(storyOnlyText(
				"*Plop*"
			));
			$('#game').append(br);

			text_count = 4087;
			return;
		}

		if (e.which == 13 && text_count == 4087){
			$('#game').append(storyText(
				name_waechterin_loana,
				"WER ZUM TEUFEL IST DAS DENN JETZT?!?!?!"
			));
			$('#game').append(br);

			text_count = 4088;
			return;
		}

		if (e.which == 13 && text_count == 4088){
			$('#game').append(storyText(
				name_who,
				"Boah, wattt ist dat denn? Das sieht ja mal echt voll schaißße aus. Isch dachte hir wär foll so pardy und so. halt so voll dungeon maßig. What is denn jetzt genau dain problem. Du hast gesagt s gäbe wurst zu essen und da bin isch natürlich sofort da und so halt."
			));
			$('#game').append(br);

			text_count = 4089;
			return;
		}

		if (e.which == 13 && text_count == 4089){
			$('#game').append(storyText(
				name_freia,
				"Hi Nadine ^^. Darf ich vorstellen: Das sind Loana und Norbert."
			));
			$('#game').append(br);

			text_count = 4090;
			return;
		}

		if (e.which == 13 && text_count == 4090){
			$('#game').append(storyText(
				name_waechterin_loana,
				"Ahhh ja. Sehr erfreut ihre Bekanntsc ....."
			));
			$('#game').append(br);

			text_count = 4091;
			return;
		}

		if (e.which == 13 && text_count == 4091){
			$('#game').append(storyText(
				name_nadine,
				"Boah eyy wie schaiße ist das denn. What sind dat für kackstelzen. Kenne ich nicht und mark isch nicht."
			));
			$('#game').append(br);

			text_count = 4092;
			return;
		}

		if (e.which == 13 && text_count == 4092){
			$('#game').append(storyText(
				name_norbert,
				"Und WOHER kennst du Freia bitteschön? O.o"
			));
			$('#game').append(br);

			text_count = 4093;
			return;
		}

		if (e.which == 13 && text_count == 4093){
			$('#game').append(storyText(
				name_freia,
				"Nadine und ich haben uns vor 7 Jahren auf einer Veganerin gegen Anti-Vegenarinen Demo getroffen. Und das meinte ich wortwörtlich *Seufz*. Ich hab von ihr eine Schinkenwurst ins Gesicht geschmissen bekommen und Sie hat nen Brokkoli abbekommen *Lach*"
			));
			$('#game').append(br);

			text_count = 4094;
			return;
		}

		if (e.which == 13 && text_count == 4094){
			$('#game').append(storyText(
				name_nadine,
				"bio isch für mich abfall. die leute die bio essen sind dick wegen dem ganzen zucka. wurst und brot haaben kain zucker und macht auch deswegen nischt dick. Ich esse gerne gesundes essen weil es gesund ist. sowas wie gekochter schinken. Schinkenwursst. leberwurst. teewurst. Ssalami. Und naturlicsc erdbeerkäse. Erdbeerkähse hat die maisten vitamine und ich ess das ja auch imme jeden tag manchmal und bin ja auch nicht dick oder dof. Und main sohn der bekomt ja auch kinderschokco lade weil da ja kindermildch drinne ist und dass ist gesund füar ihn weil das gesunt ist."
			));
			$('#game').append(br);

			text_count = 4095;
			return;
		}

		if (e.which == 13 && text_count == 4095){
			$('#game').append(storyText(
				name_freia,
				"Jeap Nadine da hast du recht ^^"
			));
			$('#game').append(br);

			text_count = 4096;
			return;
		}

		if (e.which == 13 && text_count == 4096){
			$('#game').append(storyText(
				name_waechterin_loana,
				"Norbert bitte halt mich wegen den Ganzen Rechtschreibfehlern Ö.Ö"
			));
			$('#game').append(br);

			text_count = 4097;
			return;
		}

		if (e.which == 13 && text_count == 4097){
			$('#game').append(storyText(
				name_norbert,
				"Ich weiß gerade nicht, ob ich lachen oder weinen soll (ಥ﹏ಥ)"
			));
			$('#game').append(br);

			text_count = 4098;
			return;
		}

		if (e.which == 13 && text_count == 4098){
			$('#game').append(storyText(
				name_freia,
				"Rechtschreibfehler? Wo denn das? *Genau nachguck*"
			));
			$('#game').append(br);

			text_count = 4099;
			return;
		}

		if (e.which == 13 && text_count == 4099){
			$('#game').append(storyOnlyText(
				"*5 Minuten später*"
			));
			$('#game').append(br);

			text_count = 4100;
			return;
		}

		if (e.which == 13 && text_count == 4100){
			$('#game').append(storyText(
				name_freia,
				"Ach daaaaaaa ^^"
			));
			$('#game').append(br);

			text_count = 4101;
			return;
		}

		if (e.which == 13 && text_count == 4101){
			$('#game').append(storyText(
				name_waechterin_loana,
				"I S T  D A S  I H R  E R N S T !?!?!?!?!?!!?!??!!?!?!?!?!?!?!?!?"
			));
			$('#game').append(br);

			text_count = 4102;
			return;
		}

		if (e.which == 13 && text_count == 4102){
			$('#game').append(storyText(
				name_freia,
				"Jeap, denn es wird \"doof\" geschrieben ^^. Klein weil es ein Wiewort ist und mit 2 oo weil ..... keine Ahnung, das ist einfach so *Hihi*"
			));
			$('#game').append(br);

			text_count = 4103;
			return;
		}

		if (e.which == 13 && text_count == 4103){
			$('#game').append(storyText(
				name_norbert,
				"Na immerhin hat Sie sich das bemerkt (o)_(o)"
			));
			$('#game').append(br);

			text_count = 4104;
			return;
		}

		if (e.which == 13 && text_count == 4104){
			$('#game').append(storyText(
				name_waechterin_loana,
				"Okay, von den Rechtschreibfehlern mal abgesehen *Hust* Willst du nicht noch das Rätsel lösen?!"
			));
			$('#game').append(br);

			text_count = 4105;
			return;
		}

		if (e.which == 13 && text_count == 4105){
			$('#game').append(storyText(
				name_freia,
				"Ach ja, na klaro ^^. Nadine, wenn ich bitten dürfte ^^"
			));
			$('#game').append(br);

			text_count = 4106;
			return;
		}

		if (e.which == 13 && text_count == 4106){
			$('#game').append(storyText(
				name_nadine,
				"na endlisch gibt es wiedar wwhat ordentliches zu eassne und nicht dieses bio müllkram und wso."
			));
			$('#game').append(br);

			text_count = 4107;
			return;
		}

		if (e.which == 13 && text_count == 4107){
			$('#game').append(storyOnlyText(
				"Nadine macht sich an 13,27 Kg reinste Wurst zu schaffen während Sie genüßlich schmatzt und schnalzt."
			));
			$('#game').append(br);

			text_count = 4108;
			return;
		}

		if (e.which == 13 && text_count == 4108){
			$('#game').append(storyText(
				name_waechterin_loana,
				"Als wenn man einem Wildschwein beim verzehren von Gedärmen zuschauen würde."
			));
			$('#game').append(br);

			text_count = 4109;
			return;
		}

		if (e.which == 13 && text_count == 4109){
			$('#game').append(storyText(
				name_norbert,
				"Leona hält sich sowohl die Augen, als auch den Mund zu."
			));
			$('#game').append(br);

			text_count = 4110;
			return;
		}

		if (e.which == 13 && text_count == 4110){
			$('#game').append(storyText(
				name_waechterin_loana,
				"HALT DIE SCHNAUZE NORBERT. GANZ FALSCHER ZEITPUNKT!!!! *Buarg*"
			));
			$('#game').append(br);

			text_count = 4111;
			return;
		}

		if (e.which == 13 && text_count == 4111){
			$('#game').append(storyText(
				name_nadine,
				"Fertisch. Ds war voll leckesr und so. danke ich bring dir balt ein risen *nachdenk* k .... kür.....  kürrbisch mit. Auch wenn das voll abfall ist ist das voll fair und so dann. So isch muss dann mal wider gehen ich muss wieder auf jeremypascal oufpassen. Tschö und bis das letzte mal."
			));
			$('#game').append(br);

			text_count = 4112;
			return;
		}

		if (e.which == 13 && text_count == 4112){
			$('#game').append(storyText(
				name_freia,
				"Auf wiedersehen meine gute ^^. Und es heißt: Bis zum Nächsten Mal *Zwinker*"
			));
			$('#game').append(br);

			text_count = 4113;
			return;
		}

		if (e.which == 13 && text_count == 4113){
			$('#game').append(storyText(
				name_nadine,
				"boah neah ej dat is mir zuviel gelern und so. dahs sind ja 2 neue wortear für ein tag mehr als 1. und dat mach isch net weeil ih das nicht einseh und weil ich da nicht keine luisdt draus hap."
			));
			$('#game').append(br);

			text_count = 4114;
			return;
		}

		if (e.which == 13 && text_count == 4114){
			$('#game').append(storyOnlyText(
				"Und mit einem gigantischen Plop verschwand Nadine in einer Rauchwolke. Wobei natürlich die Frage ist, ob diese von ihrem Furz oder der verkrümmung der Zeittransformartionsachse kam."
			));
			$('#game').append(br);

			text_count = 4115;
			return;
		}

		if (e.which == 13 && text_count == 4115){
			$('#game').append(storyText(
				name_waechterin_loana,
				"Wow. Einfach nur wow. Ich hoffe echt für dich, dass das Nächste Kapitel mehr Entscheidungen bereit hält O.o"
			));
			$('#game').append(br);

			text_count = 4116;
			return;
		}

		if (e.which == 13 && text_count == 4116){
			$('#game').append(storyText(
				name_norbert,
				"Okay. Dann jetzt eine Frage wo ein bisschen das Wissen getestet wird. Nicht das man am Ende nur übersprungen hat um weiterzukommen *Lach*"
			));
			$('#game').append(br);

			text_count = 4117;
			return;
		}

		if (e.which == 13 && text_count == 4117){
			$('#game').empty();
			$('#game').append(storyOnlyText(
				"Gebe hier die richtige Kombination aus Zahlen und Buchstaben ein, oder wähle die 1, um Freias RÜCKBLENDE DES TODES!!!!! zu aktivieren",
				" ",
				"1. Nadine und Freia haben sich vor <strong>???</strong> Jahren auf einer Veganerin gegen Anti-Vegenarinen Demo getroffen.",
				"2. Wie viele Wurstsorten wurden von Nadine genannt?",
				"3. Wie viel Kg hat Nadine insgesamt verspeist?",
				"4. Was war das 1. Wort, dass Nadine gesagt hat, nachdem Sie aufgetaucht ist?"
			));
			$('#game').append(br);

			text_count = 4118;
			return;
		}

		if (e.which == 13 && text_count == 4118){
			var question_4118_text = $('#text-field').val().toUpperCase();
			if(question_4118_text.length >= 1){
				if(question_4118_text == "1" || question_4118_text == "RÜCKBLENDE DES TODES"){
					$('#game').empty();
					$('#text-field').val("");
					$('#game').append(storyOnlyText(
						"Nadine und Freia haben sich vor ??? Jahren auf einer Veganerin gegen Anti-Vegenarinen Demo getroffen."
					));
					$('#game').append(br);

					text_count = 4119.10;
					return;
				}
				if(question_4118_text == "7513,27BOAH"){
					$('#game').empty();
					$('#text-field').val("");
					$('#game').append(storyText(
						name_waechterin_loana,
						"Ihr beide habt es also wirklich geschafft O.o. Also das hätte ich jetzt wirklich in keinem Sinne irgendwie erdacht oder erahnen können o.O"
					));
					$('#game').append(br);

					text_count = 4120;
					return;
				}
			}else{
				text_count = 4117;
			}
			return;
		}

		// QUESTION (RÜCKBLENDE DES TODES) [start]
		if (e.which == 13 && text_count == 4119.10){
			$('#game').append(storyText(
				name_freia,
				"Recalsa springu Nihau Brigitt"
			));
			$('#game').append(br);

			text_count = 4119.11;
			return;
		}

		if (e.which == 13 && text_count == 4119.11){
			$('#game').append(storyText(
				name_norbert,
				"Und was wird das wenn es ..."
			));
			$('#game').append(br);

			text_count = 4119.12;
			return;
		}

		if (e.which == 13 && text_count == 4119.12){
			$('#game').append(storyText(
				name_freia,
				"*hcaL* nemmokebba ilokkorB nen tah eiS dnu nemmokeb nessimhcseg thciseG sni tsruwneknihcS enie rhi nov bah hcI .*zfueS* hciltröwtrow hci etniem sad dnU .nefforteg omeD neniranegeV-itnA negeg nirenageV renie fua nerhaJ 7 rov snu nebah hci dnu enidaN"
			));
			$('#game').append(br);

			text_count = 4119.13;
			return;
		}

		if (e.which == 13 && text_count == 4119.13){
			$('#game').append(storyText(
				name_norbert,
				"*Ähhhhm* Du entzifferst - wie auch immer - aus den Wortfetzen der Rückblende eine 7."
			));
			$('#game').append(br);

			text_count = 4119.14;
			return;
		}

		if (e.which == 13 && text_count == 4119.14){
			$('#game').append(storyOnlyText(
				"Wie viele Wurtsorten wurden von Nadine genannt?"
			));
			$('#game').append(br);

			text_count = 4119.15;
			return;
		}

		if (e.which == 13 && text_count == 4119.15){
			$('#game').append(storyText(
				name_freia,
				"imalasS .tsruweet .tsruwrebel . tssruwneknihcS .neknihcs rethcokeg"
			));
			$('#game').append(br);

			text_count = 4119.16;
			return;
		}

		if (e.which == 13 && text_count == 4119.16){
			$('#game').append(storyText(
				name_norbert,
				"Na da eine 5 zu zählen ist ja wohl mal echt mehr als einfach O.o"
			));
			$('#game').append(br);

			text_count = 4119.17;
			return;
		}

		if (e.which == 13 && text_count == 4119.17){
			$('#game').append(storyOnlyText(
				"Wie viel Kg hat Nadine insgesamt verspeist?"
			));
			$('#game').append(br);

			text_count = 4119.18;
			return;
		}

		if (e.which == 13 && text_count == 4119.18){
			$('#game').append(storyText(
				name_freia,
				".tzlanhcs dnu tztamhcs hcilßüneg eiS dnerhäw neffahcs uz tsruW etsnier gK 72,31 na hcis thcam enidaN"
			));
			$('#game').append(br);

			text_count = 4119.19;
			return;
		}

		if (e.which == 13 && text_count == 4119.19){
			$('#game').append(storyText(
				name_norbert,
				"Also wenn ich das jetzt rückwärts lese, müsste da ja eigentlich 13,27 rauskommen. Wer denkt sich diese Rätsel aus? O.o"
			));
			$('#game').append(br);

			text_count = 4119.20;
			return;
		}

		if (e.which == 13 && text_count == 4119.20){
			$('#game').append(storyOnlyText(
				"Was war das 1. Wort, dass Nadine gesagt hat, nachdem Sie aufgetaucht ist?"
			));
			$('#game').append(br);

			text_count = 4119.21;
			return;
		}

		if (e.which == 13 && text_count == 4119.21){
			$('#game').append(storyText(
				name_freia,
				".tlah os dnu ad trofos hcilrütan hcsi nib ad dnu nesse uz tsruw ebäg s tgaseg tsah uD .melborp niad uaneg tztej nned si tahW .gißam noegnud llov os tlah .os dnu ydrap os llof räw rih ethcad hcsI .sua eßßiahcs llov thce lam aj theis saD ?nned tad tsi tttaw ,haoB"
			));
			$('#game').append(br);

			text_count = 4119.22;
			return;
		}

		if (e.which == 13 && text_count == 4119.22){
			$('#game').append(storyText(
				name_norbert,
				"Ähhhm. Also wenn das 1. Wort gesucht wird und wir das umdrehen ..... Dann müsste das ja eigentlich ..... Boah sein. Also wenn ich mich nicht irre."
			));
			$('#game').append(br);

			text_count = 4119.23;
			return;
		}

		if (e.which == 13 && text_count == 4119.23){
			$('#game').append(storyText(
				name_norbert,
				"Aber eigentlich müsste das Passwort so richtig sein ...... *Glaub ich*"
			));
			$('#game').append(br);

			text_count = 4119.24;
			return;
		}
		// QUESTION (RÜCKBLENDE DES TODES) [end]

		if (e.which == 13 && text_count == 4120){
			$('#game').append(storyText(
				name_waechterin_loana,
				"Also Freia dann nochmals von mir Herzlichen Glückwunsch zur besta ......"
			));
			$('#game').append(br);

			text_count = 4121;
			return;
		}

		if (e.which == 13 && text_count == 4121){
			$('#game').append(storyText(
				name_freia,
				"Siehst du Herr Springbrunnen, ich hab dir doch gesagt dass das so klappt *Lach*."
			));
			$('#game').append(br);

			text_count = 4122;
			return;
		}

		if (e.which == 13 && text_count == 4122){
			$('#game').append(storyText(
				name_springbrunnen,
				"......"
			));
			$('#game').append(br);

			text_count = 4123;
			return;
		}

		if (e.which == 13 && text_count == 4123){
			$('#game').append(storyText(
				name_freia,
				"Jeap und wie es geklappt hat. Obwohl mich natürlich wundert, dass Claudia immernoch nicht Fabian von Alina erzählt hat, dass der Schwager von Kevin letztes mit Karin's Nichte des 2. ehelichen Kindes vom Schwippschwager ....."
			));
			$('#game').append(br);

			text_count = 4124;
			return;
		}

		if (e.which == 13 && text_count == 4124){
			$('#game').append(storyText(
				name_waechterin_loana,
				"Norbert. Nimm einfach diesen ********** Talisman und verlasst bitte augenblicklich meinen Schrein. *Ruhig atme*"
			));
			$('#game').append(br);

			text_count = 4125;
			return;
		}

		if (e.which == 13 && text_count == 4125){
			$('#game').append(storyOnlyText(
				"*Freia bekommt von Loana einen goldenen Talisman, der mit Diamanten und uralten Hyroglyphen besetzt ist umgehängt*"
			));
			$('#game').append(br);

			text_count = 4126;
			return;
		}

		if (e.which == 13 && text_count == 4126){
			$('#game').append(storyText(
				name_norbert,
				"Geht das schon wieder los? Ich beherrsche die uralte Sprache der idiDsoahC nicht. Wenn du dich vielleicht erinnerst. Es gab Leute, die mussten damals 19zwölfunsdreizig in den Antiken Krieg ziehen und hatten keine Zeit, um eine so komplexe und vielschichtige Sprache zu lernen -.-"
			));
			$('#game').append(br);

			text_count = 4127;
			return;
		}

		if (e.which == 13 && text_count == 4127){
			$('#game').append(storyText(
				name_norbert,
				"Wärest du wohl so freundlich und würdest mir das Gekrakel hier übersetzen?"
			));
			$('#game').append(br);

			text_count = 4128;
			return;
		}

		if (e.which == 13 && text_count == 4128){
			$('#game').append(storyOnlyText(
				"*ɥɔɐl* snɐ ɥɔsıʇsʎɯ pun ɥɔsıdǝ lɐɯʇsɹǝ ʇɥǝıs ssɐp ,ǝɥɔɐsʇdnɐɥ ɹǝqɐ 'llos uǝqıǝɹɥɔs ɹǝıɥ ɥɔı sɐʍ ,ƃunuɥɐ ǝuıǝʞ"
			));
			$('#game').append(br);

			text_count = 4129;
			return;
		}

		if (e.which == 13 && text_count == 4129){
			$('#game').append(storyText(
				name_waechterin_loana,
				"Ach auf einmal!!! Du meldest dich 1969 Jahre nicht bei mir und dann soll ich dir einen Gefallen tun?! Vergiss es mein Guter. Frag doch deine ach so \"Kluge\" Freia!!!!"
			));
			$('#game').append(br);

			text_count = 4130;
			return;
		}

		if (e.which == 13 && text_count == 4130){
			$('#game').append(storyText(
				name_norbert,
				"Aber ... Aber .... Aber ...."
			));
			$('#game').append(br);

			text_count = 4131;
			return;
		}

		if (e.which == 13 && text_count == 4131){
			$('#game').append(storyText(
				name_freia,
				"ABBA war eine schwedische Popgruppe. Sie bestand aus den beiden Paaren Agnetha Fältskog und Björn Ulvaeus sowie Benny Andersson und Anni-Frid Lyngstad, und formierte sich 1972."
			));
			$('#game').append(br);

			text_count = 4132;
			return;
		}

		if (e.which == 13 && text_count == 4132){
			$('#game').append(storyText(
				name_waechterin_loana,
				"Und DIE soll dir in irgendeiner Art und Weise weiterhelfen? *Lach*"
			));
			$('#game').append(br);

			text_count = 4133;
			return;
		}

		if (e.which == 13 && text_count == 4133){
			$('#game').append(storyText(
				name_norbert,
				"Komm Freia, es wird Zeit dass wir gehen!!!!"
			));
			$('#game').append(br);

			text_count = 4134;
			return;
		}

		if (e.which == 13 && text_count == 4134){
			$('#game').append(storyOnlyText(
				"*\"Ich\" zieht an Freias Ärmel*"
			));
			$('#game').append(br);

			text_count = 4135;
			return;
		}

		if (e.which == 13 && text_count == 4135){
			$('#game').append(storyOnlyText(
				"*Freie unterhält sich während Sie von \"Ich\" weggezerrt wird weiter mit dem Springbrunnen*"
			));
			$('#game').append(br);

			text_count = 4136;
			return;
		}

		if (e.which == 13 && text_count == 4136){
			$('#game').append(storyText(
				name_freia,
				"Auf wiedersehen Mr. Springbrunnen ^^"
			));
			$('#game').append(br);

			text_count = 4137;
			return;
		}

		if (e.which == 13 && text_count == 4137){
			$('#game').append(storyText(
				name_springbrunnen,
				"Tschöhooo. Bis denne :D"
			));
			$('#game').append(br);

			text_count = 4138;
			return;
		}

		if (e.which == 13 && text_count == 4138){
			$('#game').append(storyText(
				name_waechterin_loana,
				"Wow. Wie bitte?!?!?! Seit 1969 Jahren bin ich jetzt hier komplett alleine und JETZT REDEST DU AUF EINMAL?!?!?!?!?!"
			));
			$('#game').append(br);

			text_count = 4139;
			return;
		}

		if (e.which == 13 && text_count == 4139){
			$('#game').append(storyText(
				name_springbrunnen,
				"Ich meine ähhh Blubb?"
			));
			$('#game').append(br);

			text_count = 4140;
			return;
		}

		if (e.which == 13 && text_count == 4140){
			$('#game').append(storyText(
				name_waechterin_loana,
				"Nein nein nein nein nein, mein Freund so leicht kommst du mir nicht davon!!!!!!!!!!!!!!"
			));
			$('#game').append(br);

			text_count = 4141;
			return;
		}

		if (e.which == 13 && text_count == 4141){
			$('#game').append(storyOnlyText(
				"*Im Hintergrund hört man wie Loana den Springbrunnen anschreit, während dieser nur vor sich hinblubbert*"
			));
			$('#game').append(br);

			text_count = 4142;
			return;
		}

		if (e.which == 13 && text_count == 4142){
			$('#game').append(storyText(
				name_freia,
				"Sollten wir den beiden nicht helfen? Anscheinend haben die beiden einen Streit *Sich Sorgen mach*"
			));
			$('#game').append(br);

			text_count = 4143;
			return;
		}

		if (e.which == 13 && text_count == 4143){
			$('#game').append(storyText(
				name_norbert,
				"Neeeeeneeee, den geht es gut. Die ähhhh spielen nur ein Spiel wer den anderen am lautesten anschreien kann."
			));
			$('#game').append(br);

			text_count = 4144;
			return;
		}

		if (e.which == 13 && text_count == 4144){
			$('#game').append(storyText(
				name_freia,
				"Ach wirklich? Cool ^^. Können wir das auch mal spielen?"
			));
			$('#game').append(br);

			text_count = 4145;
			return;
		}

		if (e.which == 13 && text_count == 4145){
			$('#game').append(storyText(
				name_norbert,
				"Später, jetzt müssen wir erst mal ins Nächste Kapitel *Lach*"
			));
			$('#game').append(br);

			text_count = 5000;
			return;
		}
		// CHAPTER 4 [end]
	});
});