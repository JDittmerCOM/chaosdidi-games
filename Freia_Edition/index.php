<!DOCTYPE html>
<html>
    <head>
        <meta name="description" content="Deutscher Let's Player mit leichtem Andrang zu Schizophrenie und Persönlichkeitsspaltungen. Meist PS3 Spiele, aber auf Wunsch auch PC.">
        <meta name="keywords" content="">

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="assets/img/favicon.png" type="image/x-icon" />

        <title>Freia Edition</title>

        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/custom.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="container-fluid">

            <div id="game_start">
                <?
                    function nb($nb){
                        $count = 0;
                        $result = "";
                        while ($count < $nb) {
                            $result .= "&nbsp;";
                            $count++;
                        }
                        return $result;
                    }

                    echo nb(1)."______".nb(11)."_".nb(10)."______".nb(5)."_".nb(2)."_".nb(2)."_".nb(4)."_".nb(15)."<br>";
                    echo "|".nb(2)."____|".nb(9)."(_)".nb(8)."|".nb(2)."____|".nb(3)."|".nb(1)."|(_)|".nb(1)."|".nb(2)."(_)".nb(14)."<br>";
                    echo "|".nb(1)."|__".nb(2)."_".nb(1)."__".nb(2)."___".nb(2)."_".nb(3)."__".nb(1)."_".nb(2)."|".nb(1)."|__".nb(4)."__|".nb(1)."|".nb(1)."_".nb(1)."|".nb(1)."|_".nb(2)."_".nb(3)."___".nb(3)."_".nb(1)."__".nb(2)."<br>";
                    echo "|".nb(2)."__||".nb(1)."'__|/".nb(1)."_".nb(1)."\|".nb(1)."|".nb(1)."/".nb(1)."_`".nb(1)."|".nb(1)."|".nb(2)."__|".nb(2)."/".nb(1)."_`".nb(1)."||".nb(1)."||".nb(1)."__||".nb(1)."|".nb(1)."/".nb(1)."_".nb(1)."\\".nb(1)."|".nb(1)."'_".nb(1)."\\".nb(1)."<br>";
                    echo "|".nb(1)."|".nb(3)."|".nb(1)."|".nb(2)."|".nb(2)."__/|".nb(1)."||".nb(1)."(_|".nb(1)."|".nb(1)."|".nb(1)."|____|".nb(1)."(_|".nb(1)."||".nb(1)."||".nb(1)."|_".nb(1)."|".nb(1)."||".nb(1)."(_)".nb(1)."||".nb(1)."|".nb(1)."|".nb(1)."|<br>";
                    echo "|_|".nb(3)."|_|".nb(3)."\___||_|".nb(1)."\__,_|".nb(1)."|______|\__,_||_|".nb(1)."\__||_|".nb(1)."\___/".nb(1)."|_|".nb(1)."|_|";
                ?>
            </div>

            <div id="current_chapter"></div>

            <div id="game"></div>

            <div id="game_text-field">
                <div class="row">
                    <div class="col-md-9 col-md-offset-3">
                        <input type="text" id="text-field" autocomplete="off" autofocus="" placeholder="Eingabe..">
                    </div>
                </div>
            </div>

            <div id="game_footer" class="text-uppercase">Developer Version<br>&copy; 2015 - ChaosDidi</div>

        </div>


        <script src="assets/js/jquery-1.11.2.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>

        <script src="assets/js/game_functions.js"></script>

        <!--<script src="assets/js/typewriter/jquery.typewriter.js"></script>-->
        <!--<script src="assets/js/typewriter/jquery.typeout.js"></script>-->

        <script src="assets/js/chapter/chapter1.js"></script>
        <script src="assets/js/chapter/chapter2.js"></script>
        <script src="assets/js/chapter/chapter3.js"></script>
        <script src="assets/js/chapter/chapter4.js"></script>
    </body>
</html>