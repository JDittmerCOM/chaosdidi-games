var text_count = 0;
var br = "<br>";

var name_freia = "<span style='color:deeppink'>Freia:</span>";
var name_norbert = "<span style='color:orange'>Norbert:</span>";
var name_guenther = "<span style='color:chartreuse'>Günther:</span>";
var name_hexe = "<span style='color:aliceblue'>Hexe:</span>";
var name_waechterin_loana = "<span style='color:palegreen'>Wächterin Loana:</span>";
var name_waechterin_loana_ente = "<span style='color:palegreen'>Loana (Ente):</span>";
var name_schwarzer_ritter = "<span style='color:dodgerblue'>Schwarzer Ritter:</span>";
var name_troll = "<span style='color:khaki'>Troll:</span>";
var name_luege_und_wahrheit = "<span style='color:beige'>Lüge und Wahrheit:</span>";
var name_geist = "<span style='color:lightgrey'>Geist:</span>";
var name_sternchen = "<span style='color:darkgreen'>*:</span>";
var name_nadine = "<span style='color:yellow'>Nadine:</span>";
var name_springbrunnen = "<span style='color:grey'>Springbrunnen:</span>";
var name_who = "<span style='color:grey'>???:</span>";
var name_aidan = "<span style='color:lime'>Aidan:</span>";
var name_didi = "<span style='color:cyan'>Didi:</span>";
var name_ich = "<span style='color:white'>Ich:</span>";


function nb(num){
	var text = "";
	var i = 0;

	while (i < num) {
        text += "&nbsp;";
        i++;
    }

    return text;
}

function storyText(name, text1, text2, text3, text4, text5, text6, text7, text8, text9, text10){
	if(typeof text2 == 'undefined'){text2 = "";}else if(text2.length > 0){text2 = "<br>"+text2;}
	if(typeof text3 == 'undefined'){text3 = "";}else if(text3.length > 0){text3 = "<br>"+text3;}
	if(typeof text4 == 'undefined'){text4 = "";}else if(text4.length > 0){text4 = "<br>"+text4;}
	if(typeof text5 == 'undefined'){text5 = "";}else if(text5.length > 0){text5 = "<br>"+text5;}
	if(typeof text6 == 'undefined'){text6 = "";}else if(text6.length > 0){text6 = "<br>"+text6;}
	if(typeof text7 == 'undefined'){text7 = "";}else if(text7.length > 0){text7 = "<br>"+text7;}
	if(typeof text8 == 'undefined'){text8 = "";}else if(text8.length > 0){text8 = "<br>"+text8;}
	if(typeof text9 == 'undefined'){text9 = "";}else if(text9.length > 0){text9 = "<br>"+text9;}
	if(typeof text10 == 'undefined'){text10 = "";}else if(text10.length > 0){text10 = "<br>"+text10;}

	return "\
		<div class='row'>\
			<div class='col-md-3 text-right'><strong>"+name+"</strong></div>\
			<div class='col-md-9'>\
				"+text1+"\
				"+text2+"\
				"+text3+"\
				"+text4+"\
				"+text5+"\
				"+text6+"\
				"+text7+"\
				"+text8+"\
				"+text9+"\
				"+text10+"\
			</div>\
		</div>\
	";
}

function storyQuestion(question, text1, text2, text3, text4, text5, text6, text7, text8, text9, text10){
	if(typeof text2 == 'undefined'){text2 = "";}else if(text2.length > 0){text2 = "<br>"+text2;}
	if(typeof text3 == 'undefined'){text3 = "";}else if(text3.length > 0){text3 = "<br>"+text3;}
	if(typeof text4 == 'undefined'){text4 = "";}else if(text4.length > 0){text4 = "<br>"+text4;}
	if(typeof text5 == 'undefined'){text5 = "";}else if(text5.length > 0){text5 = "<br>"+text5;}
	if(typeof text6 == 'undefined'){text6 = "";}else if(text6.length > 0){text6 = "<br>"+text6;}
	if(typeof text7 == 'undefined'){text7 = "";}else if(text7.length > 0){text7 = "<br>"+text7;}
	if(typeof text8 == 'undefined'){text8 = "";}else if(text8.length > 0){text8 = "<br>"+text8;}
	if(typeof text9 == 'undefined'){text9 = "";}else if(text9.length > 0){text9 = "<br>"+text9;}
	if(typeof text10 == 'undefined'){text10 = "";}else if(text10.length > 0){text10 = "<br>"+text10;}

	return "\
		<div class='row'>\
			<div class='col-md-9 col-md-offset-3'>\
				<strong><u>"+question+"</strong></u><br><br>\
				"+text1+"\
				"+text2+"\
				"+text3+"\
				"+text4+"\
				"+text5+"\
				"+text6+"\
				"+text7+"\
				"+text8+"\
				"+text9+"\
				"+text10+"\
			</div>\
		</div>\
	";
}

function storyOnlyText(text1, text2, text3, text4, text5, text6, text7, text8, text9, text10){
	if(typeof text2 == 'undefined'){text2 = "";}else if(text2.length > 0){text2 = "<br>"+text2;}
	if(typeof text3 == 'undefined'){text3 = "";}else if(text3.length > 0){text3 = "<br>"+text3;}
	if(typeof text4 == 'undefined'){text4 = "";}else if(text4.length > 0){text4 = "<br>"+text4;}
	if(typeof text5 == 'undefined'){text5 = "";}else if(text5.length > 0){text5 = "<br>"+text5;}
	if(typeof text6 == 'undefined'){text6 = "";}else if(text6.length > 0){text6 = "<br>"+text6;}
	if(typeof text7 == 'undefined'){text7 = "";}else if(text7.length > 0){text7 = "<br>"+text7;}
	if(typeof text8 == 'undefined'){text8 = "";}else if(text8.length > 0){text8 = "<br>"+text8;}
	if(typeof text9 == 'undefined'){text9 = "";}else if(text9.length > 0){text9 = "<br>"+text9;}
	if(typeof text10 == 'undefined'){text10 = "";}else if(text10.length > 0){text10 = "<br>"+text10;}

	return "\
		<div class='row'>\
			<div class='col-md-9 col-md-offset-3'>\
				"+text1+"\
				"+text2+"\
				"+text3+"\
				"+text4+"\
				"+text5+"\
				"+text6+"\
				"+text7+"\
				"+text8+"\
				"+text9+"\
				"+text10+"\
			</div>\
		</div>\
	";
}

function storyZitat(text1, text2, text3, text4, text5, text6, text7, text8, text9, text10){
	if(typeof text2 == 'undefined'){text2 = "";}else if(text2.length > 0){text2 = "<br>"+text2;}
	if(typeof text3 == 'undefined'){text3 = "";}else if(text3.length > 0){text3 = "<br>"+text3;}
	if(typeof text4 == 'undefined'){text4 = "";}else if(text4.length > 0){text4 = "<br>"+text4;}
	if(typeof text5 == 'undefined'){text5 = "";}else if(text5.length > 0){text5 = "<br>"+text5;}
	if(typeof text6 == 'undefined'){text6 = "";}else if(text6.length > 0){text6 = "<br>"+text6;}
	if(typeof text7 == 'undefined'){text7 = "";}else if(text7.length > 0){text7 = "<br>"+text7;}
	if(typeof text8 == 'undefined'){text8 = "";}else if(text8.length > 0){text8 = "<br>"+text8;}
	if(typeof text9 == 'undefined'){text9 = "";}else if(text9.length > 0){text9 = "<br>"+text9;}
	if(typeof text10 == 'undefined'){text10 = "";}else if(text10.length > 0){text10 = "<br>"+text10;}

	return "\
		<div class='row'>\
			<div class='col-md-3 text-right'><strong>Zitat:</strong></div>\
			<div class='col-md-9'>\
				<i>"+text1+"</i>\
				<i>"+text2+"</i>\
				<i>"+text3+"</i>\
				<i>"+text4+"</i>\
				<i>"+text5+"</i>\
				<i>"+text6+"</i>\
				<i>"+text7+"</i>\
				<i>"+text8+"</i>\
				<i>"+text9+"</i>\
				<i>"+text10+"</i>\
			</div>\
		</div>\
	";
}

function chapter(title){
	return "<center><u>"+title+"</u></center><br>";
}