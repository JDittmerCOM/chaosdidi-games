$(document).ready(function(){
	$('#text-field').keypress(function(e){
		$('html, body').animate({scrollTop: $(document).height()}, 50);

		// CHAPTER 5 [start]
		if (e.which == 13 && text_count == 5000){
			$('#current_chapter').empty();
			$('#current_chapter').append(chapter("Kapitel 5: Emotionale Begebenheiten"));
			
			$('#game').append(storyOnlyText(
				"*Loana im Hintergrund*: Sator arepo tenet opera rotas!!!"
			));
			$('#game').append(br);

			text_count = 5001;
			return;
		}

		if (e.which == 13 && text_count == 5001){
			$('#game').append(storyText(
				name_springbrunnen,
				"*Blubb?*"
			));
			$('#game').append(br);

			text_count = 5002;
			return;
		}

		if (e.which == 13 && text_count == 5002){
			$('#game').append(storyText(
				name_waechterin_loana,
				"Ich werde dich vernichten!!!!!!!!!!!!!!!!!!!!!!! Hör du mir bloß hier rumzublubbern!!!! Und bleib endlich stehen ich krieg dich ja trotzdem!!!!!!! Seit 1969 fucking Jahren dachte ich dass ich alleine in diesem ******************* Dungeon bin und jetzt kannst du nicht mehr reden, wo diese hohle Nuss nicht mehr da ist?!?!?!?!?!?!"
			));
			$('#game').append(br);

			text_count = 5003;
			return;
		}

		if (e.which == 13 && text_count == 5003){
			$('#game').append(storyText(
				name_freia,
				"Hat Sie eine Nuss verloren? Soll ich ihr suchen helfen? Ich kann ja meine Magie einsetzen um ...."
			));
			$('#game').append(br);

			text_count = 5004;
			return;
		}

		if (e.which == 13 && text_count == 5004){
			$('#game').append(storyText(
				name_waechterin_loana,
				"Kirst, imbi ist hûcze Nû fliuc dû vihu mînaz hera!!!!"
			));
			$('#game').append(br);

			text_count = 5005;
			return;
		}

		if (e.which == 13 && text_count == 5005){
			$('#game').empty();
			$('#game').append(storyOnlyText(
				"*Ein \"versehentlicher\" 25 Meter großer Feuerball rast auf Freia und den extrem stummen Helden zu.*"
			));
			$('#game').append(br);

			text_count = 5006;
			return;
		}

		if (e.which == 13 && text_count == 5006){
			$('#game').empty();
			$('#game').append(storyQuestion(
				"Was wollt ihr machen?",
				"D - Ducken",
				"F - Feuermagie mit Feuermagie bekämpfen",
				"E - Spezialfähigkeit 1 benutzen (Einen Enten Verwandlungszauber auf Loana wirken)"
			));
			$('#game').append(br);

			text_count = 5007;
			return;
		}

		if (e.which == 13 && text_count == 5007){
			var question_5007_text = $('#text-field').val().toUpperCase();
			if(question_5007_text.length >= 1){
				if(question_5007_text == "D" || question_5007_text == "DUCKEN"){
					$('#game').empty();
					$('#text-field').val("");
					$('#game').append(storyText(
						name_ich,
						"Ducken."
					));
					$('#game').append(br);

					text_count = 5008.10;
					return;
				}
				if(question_5007_text == "F" || question_5007_text == "FEUERMAGIE MIT FEUERMAGIE BEKÄMPFEN"){
					$('#game').empty();
					$('#text-field').val("");
					$('#game').append(storyText(
						name_ich,
						"Feuermagie mit Feuermagie bekämpfen."
					));
					$('#game').append(br);

					text_count = 5010;
					return;
				}
				if(question_5007_text == "E" || question_5007_text == "SPEZIALFÄHIGKEIT 1 BENUTZEN"){
					$('#game').empty();
					$('#text-field').val("");
					$('#game').append(storyText(
						name_ich,
						"Spezialfähigkeit 1 benutzen"
					));
					$('#game').append(br);

					text_count = 5009.10;
					return;
				}
			}else{
				text_count = 5005;
			}
			return;
		}

		//Question (Ducken) [start]
		if (e.which == 13 && text_count == 5008.10){
			$('#game').append(storyOnlyText(
				"Okay nur als Info. Wenn ich sage, dass der Feuerball 23 FUCKING METER groß ist, meinst du, du kannst dich \"mal ebend\" so wegducken? O.o"
			));
			$('#game').append(br);

			text_count = 5008.11;
			return;
		}

		if (e.which == 13 && text_count == 5008.11){
			$('#game').append(storyOnlyText(
				"E - Egal. Ich schaff das schon :D",
				"F - Feuermagie mit Feuermagie bekämpfen"
			));
			$('#game').append(br);

			text_count = 5008.12;
			return;
		}

		if (e.which == 13 && text_count == 5008.12){
			var question_5008_12_text = $('#text-field').val().toUpperCase();
			if(question_5008_12_text.length >= 1){
				if(question_5008_12_text == "E" || question_5008_12_text == "EGAL. ICH SCHAFF DAS SCHON :D"){
					$('#game').empty();
					$('#text-field').val("");
					$('#game').append(storyText(
						name_ich,
						"Egal. Ich schaff das schon :D"
					));
					$('#game').append(br);

					text_count = 5008.1310;
					return;
				}
				if(question_5008_12_text == "F" || question_5008_12_text == "FEUERMAGIE MIT FEUERMAGIE BEKÄMPFEN"){
					$('#game').empty();
					$('#text-field').val("");
					$('#game').append(storyText(
						name_ich,
						"Feuermagie mit Feuermagie bekämpfen."
					));
					$('#game').append(br);

					text_count = 5010;
					return;
				}
			}else{
				text_count = 5008.10;
			}
			return;
		}

		//Question (Ducken)(Egal. Ich schaff das schon :D) [start]
		if (e.which == 13 && text_count == 5008.1310){
			$('#game').append(storyText(
				name_norbert,
				"Beim Versuch dem Feuerball auszuweichen, verbrennst du sowohl innerlich als auch von außen. Einen Vorteil hat es ja. Deine Fettporen waren noch nie so weit geöffnet wie jetzt. Also Unreinheiten, oder Dreck ist nicht mehr vorhanden *Lach*. Ach Ja. Nur ein Tipp für's RL. Wenn dir ein Erzähler sagt, du kannst dem 23 Meter großen Feuerball nicht ausweichen, dann kannst du Nächstes mal ruhig darauf hören XD."
			));
			$('#game').append(br);

			text_count = 5008.1311;
			return;
		}

		if (e.which == 13 && text_count == 5008.1311){
			$('#game').append(storyOnlyText(
				"GAME OVER BITCH!!!!!!!!"
			));
			$('#game').append(br);

			text_count = 5008.10;
			return;
		}
		//Question (Ducken)(Egal. Ich schaff das schon :D) [end]
		//Question (Ducken) [end]
		//Question (Spezialfähigkeit 1 benutzen) [start]
		if (e.which == 13 && text_count == 5009.10){
			$('#game').append(storyText(
				name_freia,
				"Oha. Also was das wohl wird Ö.ö"
			));
			$('#game').append(br);

			text_count = 5009.11;
			return;
		}

		if (e.which == 13 && text_count == 5009.11){
			$('#game').append(storyText(
				name_freia,
				"Minekravt Mist. Fadretui kilouptrs Jagoshzuiop Minuas Frecellike!!!!!!"
			));
			$('#game').append(br);

			text_count = 5009.12;
			return;
		}

		if (e.which == 13 && text_count == 5009.12){
			$('#game').append(storyOnlyText(
				"*Der Zauber trifft Loana mit voller Bandbreite am Ganzen Körper und dieser entfaltet sich und es entstehen langsam Anzeichen von einer Ente*"
			));
			$('#game').append(br);

			text_count = 5009.13;
			return;
		}

		if (e.which == 13 && text_count == 5009.13){
			$('#game').append(storyText(
				name_freia,
				"OMG, IST DIE PUTZIG!!!!!!!!!!!!!!!!!. WO KOMMT DIE DENN HER?!?!"
			));
			$('#game').append(br);

			text_count = 5009.14;
			return;
		}

		if (e.which == 13 && text_count == 5009.14){
			$('#game').append(storyText(
				name_waechterin_loana_ente,
				"Quack?! Quack Quack quack!!!!!"
			));
			$('#game').append(br);

			text_count = 5009.15;
			return;
		}

		if (e.which == 13 && text_count == 5009.15){
			$('#game').append(storyText(
				name_freia,
				"Oh die ist ja mal echt mehr als Zuckersüß!!!!!!!!!!!!"
			));
			$('#game').append(br);

			text_count = 5009.16;
			return;
		}

		if (e.which == 13 && text_count == 5009.16){
			$('#game').append(storyOnlyText(
				"*Gutschie Gutschie gu gu*"
			));
			$('#game').append(br);

			text_count = 5009.17;
			return;
		}

		if (e.which == 13 && text_count == 5009.17){
			$('#game').append(storyText(
				name_waechterin_loana_ente,
				"Quack quack Quack Uqack Uiiii Quack QuckaQuackquack."
			));
			$('#game').append(br);

			text_count = 5009.18;
			return;
		}

		if (e.which == 13 && text_count == 5009.18){
			$('#game').append(storyText(
				name_freia,
				"Na du kleine. Na wie heißt du denn?"
			));
			$('#game').append(br);

			text_count = 5009.19;
			return;
		}

		if (e.which == 13 && text_count == 5009.19){
			$('#game').append(storyText(
				name_springbrunnen,
				"Wow, also das hätte ich jetzt nicht erwartet Ö.Ö"
			));
			$('#game').append(br);

			text_count = 5009.20;
			return;
		}

		if (e.which == 13 && text_count == 5009.20){
			$('#game').append(storyText(
				name_waechterin_loana_ente,
				"QUACK!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
			));
			$('#game').append(br);

			text_count = 5009.21;
			return;
		}

		if (e.which == 13 && text_count == 5009.21){
			$('#game').append(storyText(
				name_springbrunnen,
				"Also ich kann zwar keine Entisch, aber ich bin mir sicher sie flucht gerade rum und versucht Magie zu verwenden *Lach*"
			));
			$('#game').append(br);

			text_count = 5009.22;
			return;
		}

		if (e.which == 13 && text_count == 5009.22){
			$('#game').append(storyText(
				name_waechterin_loana_ente,
				"QUACK?!?!?!?!?!!?!?!?!?!?!?!?!?!? Quack QUACK QUACK!!!!!"
			));
			$('#game').append(br);

			text_count = 5009.23;
			return;
		}

		if (e.which == 13 && text_count == 5009.23){
			$('#game').append(storyText(
				name_springbrunnen,
				"Also jedenfalls vielen Dank für eure Hilfe. Hier nehmt das als kleine Anerkennung von mir."
			));
			$('#game').append(br);

			text_count = 5009.24;
			return;
		}

		if (e.which == 13 && text_count == 5009.24){
			$('#game').append(storyOnlyText(
				"*Der Springbrunnen reicht dir eine kleine Schatulle, aus der man ein leises flüstern hört.* Dieses Artefakt kann exakt 1. Verwendet werden und entfaltet seine wahre Macht nur in der richtigen Situation."
			));
			$('#game').append(br);

			text_count = 5009.25;
			return;
		}

		if (e.which == 13 && text_count == 5009.25){
			$('#game').append(storyText(
				name_norbert,
				"Ist das DASS, was ich denke??????? Woher hast du so ein Antikes und legendäres Artefakt?!?!?!?!?!?"
			));
			$('#game').append(br);

			text_count = 5009.26;
			return;
		}

		if (e.which == 13 && text_count == 5009.26){
			$('#game').append(storyText(
				name_springbrunnen,
				"Ich bin immerhin der HEILIGE SPRINGBRUNNEN VON TSRUDTZTEJBAHHCI. Und meine Geheimnisse werden mit mir ins Grab gehen. Es tut mir leid Großmeister Norbert."
			));
			$('#game').append(br);

			text_count = 5009.27;
			return;
		}

		if (e.which == 13 && text_count == 5009.27){
			$('#game').append(storyText(
				name_freia,
				"Heyyyy eine Ente. Wilhelm, hast du die schon bemerkt? O.o"
			));
			$('#game').append(br);

			text_count = 5009.28;
			return;
		}

		if (e.which == 13 && text_count == 5009.28){
			$('#game').append(storyText(
				name_norbert,
				"......."
			));
			$('#game').append(br);

			text_count = 5009.29;
			return;
		}

		if (e.which == 13 && text_count == 5009.29){
			$('#game').append(storyText(
				name_norbert,
				"Nein, die ist bestimmt nur kurz von zuhause weggelaufen und schwimmt gleich wieder zurück :). Es geht ihr bestimmt gut."
			));
			$('#game').append(br);

			text_count = 5009.30;
			return;
		}

		if (e.which == 13 && text_count == 5009.30){
			$('#game').append(storyText(
				name_springbrunnen,
				"Dann auf Wiedersehen und bis zum nächsten Mal."
			));
			$('#game').append(br);

			text_count = 5009.31;
			return;
		}

		if (e.which == 13 && text_count == 5009.31){
			$('#game').append(storyText(
				name_freia,
				"Tschüss Mr. Springbrunnen und grüßen Sie mir Loana, wenn Sie sie sehen. Irgendwie hat Sie sich auf einmal in Luft aufgelöst O.o"
			));
			$('#game').append(br);

			text_count = 5009.32;
			return;
		}

		if (e.which == 13 && text_count == 5009.32){
			$('#game').append(storyText(
				name_springbrunnen,
				"Na klar doch. Warte ich Teleportier euch kurz ins Nächste Kapitel *Luft hol*"
			));
			$('#game').append(br);

			text_count = 5009.33;
			return;
		}

		if (e.which == 13 && text_count == 5009.33){
			$('#game').append(storyText(
				name_springbrunnen,
				"Nihau, Kaskas, Tradeg, Uitrest, Lösptr!!!!!!!"
			));
			$('#game').append(br);

			text_count = 5009.34;
			return;
		}

		if (e.which == 13 && text_count == 5009.34){
			$('#game').append(storyOnlyText(
				"*Du wirst direkt nach Kapitel 6 Teleportiert*"
			));
			$('#game').append(br);

			text_count = 6000;
			return;
		}
		//Question (Spezialfähigkeit 1 benutzen) [end]

		if (e.which == 13 && text_count == 5010){
			$('#game').append(storyText(
				name_freia,
				"Meinst du wirklich, ich habe eine Chance gegen diese hochangesehende Wächterin?"
			));
			$('#game').append(br);

			text_count = 5011;
			return;
		}
		// CHAPTER 5 [end]
	});
});