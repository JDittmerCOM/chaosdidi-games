$(document).ready(function(){
	$('#text-field').keypress(function(e){
		$('html, body').animate({scrollTop: $(document).height()}, 50);

		// CHAPTER 1 [start]
		if (e.which == 13 && text_count == 0){
			$('#game_start').empty();
			$('#game').empty();
			$('#current_chapter').empty();
			$('#current_chapter').append(chapter("Kapitel 1: Die Ankunft"));

			text_count = 1000;
			return;
		}

		if (e.which == 13 && text_count == 1000){
			$('#game').append(storyText(
				"Prolog:", 
				"Nachdem du Wilhelm genug genervt hast, findest du dich auf einer einsamen Lichtung wieder. Er kann dir nur im Geiste weiter helfen, den Rest musst du alleine schaffen. Freia hat natürlich auch Spezialfähigkeiten, die manchmal hilfreich und manchmal nur unnötig, dafür aber amüsant sind. Du darfst insgesamt bis zum Ende diese nur X Mal (Da überlege ich mir noch einen Wert). Auch wenn du stirbst, also überleg es dir gut. Wenn du eine Fähigkeit bereits benutzt hast, darfst du diese natürlich auch nach deinem Tot wieder bei der Entscheidung wählen. Aber sei ehrlich zu dir selbst, wie oft du was benutzt hast ;)",
				" ",
				"Viel Spaß ^^"
			));
			$('#game').append(br);

			text_count = 1001;
			return;
		}

		if (e.which == 13 && text_count == 1001){
			$('#game').empty();
			$('#game').append(storyText(
				name_freia, 
				"Heyyyyy was mach ich denn hier?"
			));
			$('#game').append(br);

			text_count = 1002;
			return;
		}

		if (e.which == 13 && text_count == 1002){
			$('#game').append(storyText(
				name_norbert, 
				"Du bist nun auf dich gestellt. Alles was du tust, was du siehst und was du weißt wird von nun an Konsequenzen haben."
			));
			$('#game').append(br);

			text_count = 1003;
			return;
		}

		if (e.which == 13 && text_count == 1003){
			$('#game').append(storyText(
				name_freia, 
				"Oh man, das wirkt ja fast wie ein echt doofes Tutorial *Seufz*"
			));
			$('#game').append(br);

			text_count = 1004;
			return;
		}

		if (e.which == 13 && text_count == 1004){
			$('#game').append(storyText(
				name_norbert,
				"HEY, DAS IST KEIN DOOFES TUTORIAL!!!"
			));
			$('#game').append(br);

			text_count = 1005;
			return;
		}

		if (e.which == 13 && text_count == 1005){
			$('#game').append(storyText(
				name_freia,
				"Und was muss ich hier machen?"
			));
			$('#game').append(br);

			text_count = 1006;
			return;
		}

		if (e.which == 13 && text_count == 1006){
			$('#game').append(storyText(
				name_norbert,
				"Also zuerst mal, brauchst du ein Blatt Papier und ein Stift. Oder ein echt gutes Gedächtnis. Denn in der Welt von Didi ist nichts so wie es scheint. Und da die Fantasie einer einzelnen Person bekanntlicherweise unbegrenzt ist, kannst du dir das bei Didi ja ausmalen, was so auf dich zukommt."
			));
			$('#game').append(br);

			text_count = 1007;
			return;
		}

		if (e.which == 13 && text_count == 1007){
			$('#game').append(storyText(
				name_freia,
				"Oha. Also ich bin ja schon recht Kluk. Also ich glaube ich schaffe das ^^"
			));
			$('#game').append(br);

			text_count = 1008;
			return;
		}

		if (e.which == 13 && text_count == 1008){
			$('#game').append(storyText(
				name_norbert,
				"Das heißt klug -.-. Also kleingeschrieben und mit einem g und keinem k *Seufz*"
			));
			$('#game').append(br);

			text_count = 1009;
			return;
		}

		if (e.which == 13 && text_count == 1009){
			$('#game').append(storyText(
				name_freia,
				"Ja mein ich doooooooooch."
			));
			$('#game').append(br);

			text_count = 1010;
			return;
		}

		if (e.which == 13 && text_count == 1010){
			$('#game').append(storyText(
				name_norbert,
				"Jedenfalls bist du auf einer einsam ......"
			));
			$('#game').append(br);

			text_count = 1011;
			return;
		}

		if (e.which == 13 && text_count == 1011){
			$('#game').append(storyText(
				name_freia,
				"Warum bin ich denn einsam?"
			));
			$('#game').append(br);

			text_count = 1012;
			return;
		}

		if (e.which == 13 && text_count == 1012){
			$('#game').append(storyText(
				name_norbert,
				"Ähhh. Weil das hier sonst zu einfach wäre."
			));
			$('#game').append(br);

			text_count = 1013;
			return;
		}

		if (e.which == 13 && text_count == 1013){
			$('#game').append(storyText(
				name_norbert,
				"Also du bist auf einer einsamen Lichtung und bist eine tapfere Kriegerin, die ...."
			));
			$('#game').append(br);

			text_count = 1014;
			return;
		}

		if (e.which == 13 && text_count == 1014){
			$('#game').append(storyText(
				name_freia,
				"DUUUUUUUUUU?"
			));
			$('#game').append(br);

			text_count = 1015;
			return;
		}

		if (e.which == 13 && text_count == 1015){
			$('#game').append(storyText(
				name_norbert,
				"Was ist denn jetzt schon wieder?!"
			));
			$('#game').append(br);

			text_count = 1016;
			return;
		}

		if (e.which == 13 && text_count == 1016){
			$('#game').append(storyText(
				name_freia,
				"Ich will keine Kriegerin sein, sondern eine Magierin *Augen Zwinkern*"
			));
			$('#game').append(br);

			text_count = 1017;
			return;
		}

		if (e.which == 13 && text_count == 1017){
			$('#game').append(storyText(
				name_norbert,
				"ABER JETZT IST DAS GANZE DESIGN SCHON AUF KRIEGER EINGESTELLT!!!!!!"
			));
			$('#game').append(br);

			text_count = 1018;
			return;
		}

		if (e.which == 13 && text_count == 1018){
			$('#game').append(storyText(
				name_freia,
				"Hey, ich dachte du bist der große Dungeon Meister von 19zwölfunddreizig. Dann sollte es doch wohl eine kleinigkeit für dich sein, das alles umzuschreiben oder ^^"
			));
			$('#game').append(br);

			text_count = 1019;
			return;
		}

		if (e.which == 13 && text_count == 1019){
			$('#game').append(storyText(
				name_norbert,
				".........."
			));
			$('#game').append(br);

			text_count = 1020;
			return;
		}

		if (e.which == 13 && text_count == 1020){
			$('#game').append(storyText(
				name_norbert,
				"Gang ut, nesso, mid nigun nessiklinon,",
				"ut fana themo marge an that ben,",
				"fan themo bene an that flesg,",
				"ut fan themo flesge a thia hud,",
				"ut fan thera hud an thesa strala!",
				"Drohtin, vethe so!"
			));
			$('#game').append(br);

			text_count = 1021;
			return;
		}

		if (e.which == 13 && text_count == 1021){
			$('#game').append(storyText(
				name_freia,
				"Und was war das jetzt bitte? O.o"
			));
			$('#game').append(br);

			text_count = 1022;
			return;
		}

		if (e.which == 13 && text_count == 1022){
			$('#game').append(storyText(
				name_norbert,
				"Ich habe MAL EBEND das gesamte Design des Dungeon's auf Magierin eingestellt, ich hoffe du bist jetzt zufrieden ......."
			));
			$('#game').append(br);

			text_count = 1023;
			return;
		}

		if (e.which == 13 && text_count == 1023){
			$('#game').append(storyText(
				name_freia,
				"Ich bin jetzt also eine ECHTE Magierin?"
			));
			$('#game').append(br);

			text_count = 1024;
			return;
		}

		if (e.which == 13 && text_count == 1024){
			$('#game').append(storyText(
				name_norbert,
				"Ja, so wie du es gewollt hast *Seufz*"
			));
			$('#game').append(br);

			text_count = 1025;
			return;
		}

		if (e.which == 13 && text_count == 1025){
			$('#game').append(storyText(
				name_freia,
				"Oh man, das ist ja mal MEGA AWESOME."
			));
			$('#game').append(br);

			text_count = 1026;
			return;
		}

		if (e.which == 13 && text_count == 1026){
			$('#game').append(storyText(
				name_freia,
				"Also hab ich jetzt alle Fähigkeiten einer echten Magierin?"
			));
			$('#game').append(br);

			text_count = 1027;
			return;
		}

		if (e.which == 13 && text_count == 1027){
			$('#game').append(storyText(
				name_norbert,
				"Oh Man ...... Worauf hab ich mich da nur eingelassen O.o"
			));
			$('#game').append(br);

			text_count = 1028;
			return;
		}

		if (e.which == 13 && text_count == 1028){
			$('#game').append(storyText(
				name_freia,
				"Guck mal was ich kann *Feuerball aus der Handfläche schieß*"
			));
			$('#game').append(br);

			text_count = 1029;
			return;
		}

		if (e.which == 13 && text_count == 1029){
			$('#game').append(storyText(
				name_norbert,
				"Wow. Sind wir jetzt hier fertig? Du willst doch irgendwann mal anfangen oder?"
			));
			$('#game').append(br);

			text_count = 1030;
			return;
		}

		if (e.which == 13 && text_count == 1030){
			$('#game').append(storyText(
				name_freia,
				"Kann ich vielleicht auch ein bisschen Brot haben?"
			));
			$('#game').append(br);

			text_count = 1031;
			return;
		}

		if (e.which == 13 && text_count == 1031){
			$('#game').append(storyText(
				name_norbert,
				"WARUM ZUM TEUFEL WILLST DU BROT?!?!?!"
			));
			$('#game').append(br);

			text_count = 1032;
			return;
		}

		if (e.which == 13 && text_count == 1032){
			$('#game').append(storyText(
				name_freia,
				"Naja, es hält sich ewig und wenn ich auf eine Ente treffe, dann kann ich Sie füttern ^^"
			));
			$('#game').append(br);

			text_count = 1033;
			return;
		}

		if (e.which == 13 && text_count == 1033){
			$('#game').append(storyText(
				name_norbert,
				"Wow ...... Einfach nur Wow ..... Okay Okay, aber dann gibt es keine Wünsche mehr in Ordnung? -.-"
			));
			$('#game').append(br);

			text_count = 1034;
			return;
		}

		if (e.which == 13 && text_count == 1034){
			$('#game').append(storyText(
				name_freia,
				"Okay ^^"
			));
			$('#game').append(br);

			text_count = 1035;
			return;
		}

		if (e.which == 13 && text_count == 1035){
			$('#game').append(storyText(
				name_norbert,
				"Suma haft heftidun, suma heri lézidun"
			));
			$('#game').append(br);

			text_count = 1036;
			return;
		}

		if (e.which == 13 && text_count == 1036){
			$('#game').append(storyText(
				name_freia,
				"Wow, du brauchst echt Magie um ein Brot zu beschwören? ^^"
			));
			$('#game').append(br);

			text_count = 1037;
			return;
		}

		if (e.which == 13 && text_count == 1037){
			$('#game').append(storyText(
				name_norbert,
				"Didi. Bitte tu mir ein Gefallen und töte mich ....."
			));
			$('#game').append(br);

			text_count = 1038;
			return;
		}

		if (e.which == 13 && text_count == 1038){
			$('#game').append(storyText(
				name_didi,
				"Nope. Das kannst du vergessen. Du musst erst das Game mit ihr durchzocken *Muahahahahhahaha*"
			));

			$('#game').append(br);

			text_count = 1039;
			return;
		}

		if (e.which == 13 && text_count == 1039){
			$('#game').append(storyText(
				name_norbert,
				"Du weißt schon, dass das Game mehr als 5 Minuten dauert oder?"
			));
			$('#game').append(br);

			text_count = 1040;
			return;
		}

		if (e.which == 13 && text_count == 1040){
			$('#game').append(storyText(
				name_didi,
				"Nope. Denn während der Text hier noch geschrieben wird, denke ich mir erst den weiteren Verlauf aus *Lach*"
			));
			$('#game').append(br);

			text_count = 1041;
			return;
		}

		if (e.which == 13 && text_count == 1041){
			$('#game').append(storyText(
				name_freia,
				"Huhu Didi. Wo bist du denn? Ich kann dich nur hören aber nicht sehen *Schnüff*"
			));
			$('#game').append(br);

			text_count = 1042;
			return;
		}

		if (e.which == 13 && text_count == 1042){
			$('#game').append(storyText(
				name_didi,
				"Ich bin hier oben im Office. Du kannst mich nicht sehen O.o"
			));
			$('#game').append(br);

			text_count = 1043;
			return;
		}

		if (e.which == 13 && text_count == 1043){
			$('#game').append(storyText(
				name_freia,
				"Office? Wo ist das denn? Ist das etwa da wo auch die Claudia letztes ihr Kleid ...."
			));
			$('#game').append(br);

			text_count = 1044;
			return;
		}

		if (e.which == 13 && text_count == 1044){
			$('#game').append(storyText(
				name_norbert,
				"ES REICHT. HIER KOMMT DIE 1. ENTSCHEIDUNG!!!!!"
			));
			$('#game').append(br);

			text_count = 1045;
			return;
		}

		if (e.which == 13 && text_count == 1045){
			$('#game').empty();
			$('#game').append(storyQuestion(
				"Willst du weiterspielen?",
				"J - Ja",
				"N - Nein"
			));
			$('#game').append(br);

			text_count = 1046;
			return;
		}

		if (e.which == 13 && text_count == 1046){
			var question_1046_text = $('#text-field').val().toUpperCase();
			if(question_1046_text.length >= 1){
				if(question_1046_text == "J" || question_1046_text == "JA"){
					$('#game').empty();
					$('#text-field').val("");
					$('#game').append(storyText(
						name_norbert,
						"Es scheint fast so, als wenn das Chaos seinen Lauf nehmen würde O.o."
					));
					$('#game').append(br);

					text_count = 1048;
					return;
				}
				if(question_1046_text == "N" || question_1046_text == "NEIN"){
					$('#game').empty();
					$('#text-field').val("");
					$('#game').append(storyText(
						name_norbert,
						"Oh Nein, jetzt müssen wir das Disaster mit Freia jetzt schon abbrechen? Das ist wirklich zu schade *Seufz*"
					));
					$('#game').append(br);

					text_count = 1047.10;
					return;
				}
			}else{
				text_count = 1045;
			}
			return;
		}

			// QUESTION 1 (NEIN) [start]
			if (e.which == 13 && text_count == 1047.10){
				$('#game').append(storyText(
					name_freia,
					"Oh Man, naja vielleicht ja beim letzten Mal"
				));
				$('#game').append(br);

				text_count = 1047.11;
				return;
			}

			if (e.which == 13 && text_count == 1047.11){
				$('#game').append(storyText(
					name_norbert,
					"Es heißt beim Nächsten Mal und nicht beim letzen Mal."
				));
				$('#game').append(br);

				text_count = 1047.12;
				return;
			}

			if (e.which == 13 && text_count == 1047.12){
				$('#game').append(storyText(
					name_freia,
					"Was Echt? Oh Man, also manchmal bin ich ja echt Dof ^^"
				));
				$('#game').append(br);

				text_count = 1047.13;
				return;
			}

			if (e.which == 13 && text_count == 1047.13){
				$('#game').append(storyText(
					name_norbert,
					"Wie bist du?: d  o  o  f"
				));
				$('#game').append(br);

				text_count = 1047.14;
				return;
			}

			if (e.which == 13 && text_count == 1047.14){
				$('#game').append(storyText(
					name_freia,
					"Ja ich weiß, aber das hab ich doch gesagt ^^"
				));
				$('#game').append(br);

				text_count = 1047.15;
				return;
			}

			if (e.which == 13 && text_count == 1047.15){
				$('#game').append(storyText(
					name_norbert,
					"Didi, ist die wirklich so doof, oder weiß Sie das echt nicht?!"
				));
				$('#game').append(br);

				text_count = 1047.16;
				return;
			}

			if (e.which == 13 && text_count == 1047.16){
				$('#game').append(storyText(
					name_didi,
					"Hat der Gamer nicht Nein geklickt? Dann sollte das doch jetzt zuende sein, oder nicht?"
				));
				$('#game').append(br);

				text_count = 1047.17;
				return;
			}

			if (e.which == 13 && text_count == 1047.17){
				$('#game').append(storyText(
					name_norbert,
					"Ach ja. GAME OVER BITCH!!!!!!"
				));
				$('#game').append(br);

				text_count = 0;
				return;
			}
			// QUESTION 1 (NEIN) [end]

		if (e.which == 13 && text_count == 1048){
			$('#game').append(storyText(
				name_freia,
				"Was nimmt wo dran Teil? Kann ich bei diesem Lauf auch mitmachen? In der Schule war ich damals eine der schnellsten Läuferinen überhaupt. Und das bei genau 22 Mitschülern."
			));
			$('#game').append(br);

			text_count = 1049;
			return;
		}

		if (e.which == 13 && text_count == 1049){
			$('#game').append(storyText(
				name_norbert,
				"Du warst mal bei irgendwas die Beste?!"
			));
			$('#game').append(br);

			text_count = 1050;
			return;
		}

		if (e.which == 13 && text_count == 1050){
			$('#game').append(storyText(
				name_freia,
				"Ja. Also ich war zu mindestens das Beste Mädchen in der Klasse ^^"
			));
			$('#game').append(br);

			text_count = 1051;
			return;
		}

		if (e.which == 13 && text_count == 1051){
			$('#game').empty();
			$('#game').append(storyText(
				name_norbert,
				"Lass mich raten: Der Rest war ......",
				" ",
				"J - Jungen",
				"T - Tiere"
			));
			$('#game').append(br);

			text_count = 1052;
			return;
		}

		if (e.which == 13 && text_count == 1052){
			var question_1052_text = $('#text-field').val().toUpperCase();
			if(question_1052_text.length >= 1){
				if(question_1052_text == "J" || question_1052_text == "JUNGEN"){
					$('#game').empty();
					$('#text-field').val("");
					$('#game').append(storyText(
						name_norbert,
						"Jungen. Und da der Rest aus deiner Klasse Jungen war, warst du das Beste Mädchen. Hab ich recht?"
					));
					$('#game').append(br);

					text_count = 1054;
					return;
				}
				if(question_1052_text == "T" || question_1052_text == "TIERE"){
					$('#game').empty();
					$('#text-field').val("");
					$('#game').append(storyText(
						name_ich,
						"Tiere und deswegen warst du das Beste Mädchen. Hab ich recht?"
					));
					$('#game').append(br);

					text_count = 1053.10;
					return;
				}
			}else{
				text_count = 1051;
			}
			return;
		}

			// QUESTION 2 (NEIN) [start]
			if (e.which == 13 && text_count == 1053.10){
				$('#game').append(storyText(
					name_freia,
					"...."
				));
				$('#game').append(br);

				text_count = 1053.11;
				return;
			}

			if (e.which == 13 && text_count == 1053.11){
				$('#game').append(storyText(
					name_freia,
					"Uglúk u bagronk sha ...."
				));
				$('#game').append(br);

				text_count = 1053.12;
				return;
			}

			if (e.which == 13 && text_count == 1053.12){
				$('#game').append(storyText(
					name_norbert,
					"Seit wann beherrscht du die Uralte Sprache der ididsoahc?!?!"
				));
				$('#game').append(br);

				text_count = 1053.13;
				return;
			}

			if (e.which == 13 && text_count == 1053.13){
				$('#game').append(storyText(
					name_freia,
					"Pushdug Saruman-glob búbhosh ska!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
				));
				$('#game').append(br);

				text_count = 1053.14;
				return;
			}

			if (e.which == 13 && text_count == 1053.14){
				$('#game').append(storyText(
					name_norbert,
					"Ähmmm ja. Also ich glaube du bist Tot. Man sollte NIE Freia reizen O.o. Und du hast mal ebend nen Riesigen Meteoriten des TODES!!!!!!!!!! voll auf die Kinnlade bekommen.",
					"GAME OVER BITCH!!!!"
				));
				$('#game').append(br);

				text_count = 0;
				return;
			}
			// QUESTION 2 (NEIN) [end]

		if (e.which == 13 && text_count == 1054){
			$('#game').append(storyText(
				name_freia,
				"Ja Genau, woher wusstest du das?"
			));
			$('#game').append(br);

			text_count = 1055;
			return;
		}

		if (e.which == 13 && text_count == 1055){
			$('#game').append(storyText(
				name_norbert,
				"Oh nur geraten"
			));
			$('#game').append(br);

			text_count = 1056;
			return;
		}

		if (e.which == 13 && text_count == 1056){
			$('#game').append(storyText(
				name_freia,
				"Ja, da hast du Recht ^^"
			));
			$('#game').append(br);

			text_count = 2000;
			return;
		}
		// CHAPTER 1 [end]
	});
});