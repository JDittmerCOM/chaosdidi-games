<div class="box">
	<h2>Kontostand (<? echo CREDITS." Pkt."; ?>) <i class="fa fa-arrow-right fa-fw"></i> Kontoauszüge</h2>
	<br>
	<table class="table">
	  	<thead>
			<tr>
		  		<th class="text-center">#ID</th>
		  		<th>Datum</th>
		  		<th>Beschreibung</th>
		  		<th>Punkte</th>
			</tr>
	  	</thead>
	  	<tbody>
	  	<?
	  		$sql = mysqli_query($db, "SELECT * FROM balance_statements WHERE userID = '".ID."' ORDER BY id DESC");
	  		while($row = mysqli_fetch_assoc($sql)){
	  			echo "<tr>";
	  				echo "<td class='text-center'>".$row['id']."</td>";
	  				echo "<td>".date("d.m.Y H:i", $row['date'])." Uhr</td>";
	  				echo "<td>".$row['description']."</td>";
	  				echo "<td>".$row['balance']."</td>";
	  			echo "</tr>";
	  		}
	  	?>
	  	</tbody>
	</table>
</div>