$(document).ready(function(){
	$('#text-field').keypress(function(e){
		$('html, body').animate({scrollTop: $(document).height()}, 50);

		// CHAPTER 2 [start]
		if (e.which == 13 && text_count == 2000){
			$('#current_chapter').empty();
			$('#current_chapter').append(chapter("Kapitel 2: Die Ersten WICHTIGEN Entscheidungen"));
			
			$('#game').append(storyText(
				name_norbert,
				"Also du bist auf einer friedlichen und ruhigen Lichtung."
			));
			$('#game').append(br);

			text_count = 2001;
			return;
		}

		if (e.which == 13 && text_count == 2001){
			$('#game').append(storyText(
				name_freia,
				"Schön hier. Vielleicht bau ich mir hier ein Häuschen hin und da kommt die Rutsche. Neben meinem Kiosk bau ich mir noch ein Schwimmingpool *Still rum murmel* *In der Luft rumzeichne*"
			));
			$('#game').append(br);

			text_count = 2002;
			return;
		}

		if (e.which == 13 && text_count == 2002){
			$('#game').append(storyText(
				name_norbert,
				"Vor dir ist eine Tür, eine Gelbe Vase und ein großer Stein."
			));
			$('#game').append(br);

			text_count = 2003;
			return;
		}

		if (e.which == 13 && text_count == 2003){
			$('#game').empty();
			$('#game').append(storyText(
				name_norbert,
				"Vor dir ist eine Tür, eine Gelbe Vase und ein großer Stein."
			));
			$('#game').append(br);

			$('#game').append(storyQuestion(
				"Was tust du?",
				"1 - Spezialfähigkeit 1 Benutzen (Meteoritenhagel DES TODES!!!!!)",
				"2 - Spezialfähigkeit 2 Benutzen (Schlüsseldienst anrufen)",
				"T - Versuchen Tür zu öffnen",
				"G - Gelbe Vase durchsuchen",
				"S - Den Stein untersuchen",
				"(????) - Schlüssel benutzen"
			));
			$('#game').append(br);

			text_count = 2004;
			return;
		}

		if (e.which == 13 && text_count == 2004){
			var question_2004_text = $('#text-field').val().toUpperCase();
			if(question_2004_text.length >= 1){
				if(question_2004_text == "1" || question_2004_text == "SPEZIALFÄHIGKEIT 1 BENUTZEN"){
					$('#game').empty();
					$('#text-field').val("");
					$('#game').append(storyText(
						name_freia,
						"Also ich glaube, die Antwort ist doch offensichtlich. Ich helf dir natürlich die Tür zu öffnen ^^"
					));
					$('#game').append(br);

					text_count = 2005.10;
					return;
				}
				if(question_2004_text == "2" || question_2004_text == "SPEZIALFÄHIGKEIT 2 BENUTZEN"){
					$('#game').empty();
					$('#text-field').val("");
					$('#game').append(storyText(
						name_ich,
						"Tiere und deswegen warst du das Beste Mädchen. Hab ich recht?"
					));
					$('#game').append(br);

					text_count = 2006.10;
					return;
				}
				if(question_2004_text == "T" || question_2004_text == "VERSUCHEN TÜR ZU ÖFFNEN"){
					$('#game').empty();
					$('#text-field').val("");
					$('#game').append(storyText(
						name_norbert,
						"Okay Freia, du willst also die Tür öffnen?"
					));
					$('#game').append(br);

					text_count = 2006.10;
					return;
				}
				if(question_2004_text == "G" || question_2004_text == "GELBE VASE DURCHSUCHEN"){
					$('#game').empty();
					$('#text-field').val("");
					$('#game').append(storyText(
						name_norbert,
						"Na dann lass uns mal schauen, was du da schönes ...."
					));
					$('#game').append(br);

					text_count = 2007.10;
					return;
				}
				if(question_2004_text == "S" || question_2004_text == "DEN STEIN UNTERSUCHEN"){
					$('#game').empty();
					$('#text-field').val("");
					$('#game').append(storyText(
						name_norbert,
						"Du untersuchst den Stein und findest einen Schlüssel mit einer 2602 eingraviert."
					));
					$('#game').append(br);

					text_count = 2008.10;
					return;
				}
				if(question_2004_text == "2602"){
					$('#game').empty();
					$('#text-field').val("");
					$('#game').append(storyOnlyText(
						"Die Tür wurde geöffnet und es scheint so, als wenn eine Dunkle Aura auf der anderen Seite der Tür lauern würde. Ungeachtet dessen - und weil du keine andere Möglichkeit hast - schreitet ihr hindurch."
					));
					$('#game').append(br);

					text_count = 3000;
					return;
				}
			}else{
				text_count = 2003;
			}
			return;
		}

		// QUESTION 3 (SPEZIALFÄHIGKEIT 1) [start]
		if (e.which == 13 && text_count == 2005.10){
			$('#game').append(storyText(
				name_norbert,
				"Ähmmmmm WASSSSS?!?!?!?!?"
			));
			$('#game').append(br);

			text_count = 2005.11;
			return;
		}

		if (e.which == 13 && text_count == 2005.11){
			$('#game').append(storyText(
				name_freia,
				"Naja, ich bin eine Magierin. Das hatten wir doch schon geklärt. Und deswegen kann ich logischerweise auch einen Meteoritenhagel DES TODES!!!!! beschwören *Hihi*"
			));
			$('#game').append(br);

			text_count = 2005.12;
			return;
		}

		if (e.which == 13 && text_count == 2005.12){
			$('#game').append(storyText(
				name_norbert,
				"Wow. Also du machst ja echt die komplette Spielmechanik kaputt oder?"
			));
			$('#game').append(br);

			text_count = 2005.13;
			return;
		}

		if (e.which == 13 && text_count == 2005.13){
			$('#game').append(storyText(
				name_freia,
				"Wieso denn das? Ich halt mich nur an die Goldene Regel von ganz oben ^^"
			));
			$('#game').append(storyZitat(
				"Freia hat natürlich auch Spezialfähigkeiten, die manchmal hilfreich und manchmal nur unnötig, dafür aber amüsant sind."
			));
			$('#game').append(br);

			text_count = 2005.14;
			return;
		}

		if (e.which == 13 && text_count == 2005.14){
			$('#game').append(storyText(
				name_freia,
				"Und ich denke mal, son Meteoritenhagel DES TODES!!!!! ist schon recht praktisch. *Hihi*"
			));
			$('#game').append(br);

			text_count = 2005.15;
			return;
		}

		if (e.which == 13 && text_count == 2005.15){
			$('#game').append(storyText(
				name_norbert,
				"Wow, einfach nur Wow. Okay okay okay. Na dann mal los. Du kennst eh nicht die richtige Formel *Lach*"
			));
			$('#game').append(br);

			text_count = 2005.16;
			return;
		}

		if (e.which == 13 && text_count == 2005.16){
			$('#game').append(storyText(
				name_freia,
				"Uglúk u bagronk sha ...."
			));
			$('#game').append(br);

			text_count = 2005.17;
			return;
		}

		if (e.which == 13 && text_count == 2005.17){
			$('#game').append(storyText(
				name_norbert,
				"Seit wann beherrscht du die Uralte Sprache der Sdidsoahc?!?!"
			));
			$('#game').append(br);

			text_count = 2005.18;
			return;
		}

		if (e.which == 13 && text_count == 2005.18){
			$('#game').append(storyText(
				name_freia,
				"Pushdug Saruman-glob búbhosh ska!!!!!!!!!"
			));
			$('#game').append(br);

			text_count = 2005.19;
			return;
		}

		if (e.which == 13 && text_count == 2005.19){
			$('#game').append(storyOnlyText(
				"Der METEORIEHAGEL DES TODES!!!!! öffnet dir natürlich wie es zu erwarten war die Tür. Du kannst nun hindurch schreiten."
			));
			$('#game').append(br);

			text_count = 2005.20;
			return;
		}

		if (e.which == 13 && text_count == 2005.20){
			$('#game').append(storyOnlyText(
				"Gebe <strong>2602</strong> ein um fortzufahren."
			));

			text_count = 2005.21;
			return;
		}

		if (e.which == 13 && text_count == 2005.21){
			var question3_2005_21_2602_text = $('#text-field').val().toUpperCase();
			if(question3_2005_21_2602_text.length == 4){
				if(question3_2005_21_2602_text == "2602"){
					$('#game').empty();
					$('#text-field').val("");
					$('#game').append(storyOnlyText(
						"Die Tür wurde geöffnet und es scheint so, als wenn eine Dunkle Aura auf der anderen Seite der Tür lauern würde. Ungeachtet dessen - und weil du keine andere Möglichkeit hast - schreitet ihr hindurch."
					));
					$('#game').append(br);

					text_count = 3000;
				}
			}
			return;
		}
		// QUESTION 3 (SPEZIALFÄHIGKEIT 1) [end]

		// QUESTION 3 (SPEZIALFÄHIGKEIT 2) [start]
		if (e.which == 13 && text_count == 2006.10){
			$('#game').append(storyText(
				name_norbert,
				"O.o. Was les ich da?! O.O O.O"
			));
			$('#game').append(br);

			text_count = 2006.11;
			return;
		}

		if (e.which == 13 && text_count == 2006.11){
			$('#game').append(storyText(
				name_freia,
				"Na ich ruf den Schlüsseldienst an ^^. Dann kann unser neuer Freund hier gleich weiter machen und muss nicht erst ein Rätsel oder so lösen *Hihi*"
			));
			$('#game').append(br);

			text_count = 2006.12;
			return;
		}

		if (e.which == 13 && text_count == 2006.12){
			$('#game').append(storyText(
				name_norbert,
				"Aber ...... WAS IST DAS FÜR EINE LOGIK?!?!?!?!?!?!?!?!?!?!?!? DU BIST IN EINEM DUNGEON 200000 METER UNTER DER ERDE. HIER HAST DU EH KEINEN EMPFANG UM ...."
			));
			$('#game').append(br);

			text_count = 2006.13;
			return;
		}

		if (e.which == 13 && text_count == 2006.13){
			$('#game').append(storyOnlyText(
				"*Telefonnummer des Schlüsseldienstes wähl*"
			));
			$('#game').append(br);

			text_count = 2006.14;
			return;
		}

		if (e.which == 13 && text_count == 2006.14){
			$('#game').append(storyText(
				name_freia,
				"Psssst. Es klingelt."
			));
			$('#game').append(br);

			text_count = 2006.15;
			return;
		}

		if (e.which == 13 && text_count == 2006.15){
			$('#game').append(storyText(
				name_norbert,
				"Didi, warum?!"
			));
			$('#game').append(br);

			text_count = 2006.16;
			return;
		}

		if (e.which == 13 && text_count == 2006.16){
			$('#game').append(storyText(
				name_didi,
				"Naja ..... Lw und so ;D"
			));
			$('#game').append(br);

			text_count = 2006.17;
			return;
		}

		if (e.which == 13 && text_count == 2006.17){
			$('#game').append(storyText(
				name_freia,
				"Psssssssssssssssssst. Sonst kann ich nichts verstehen, sobald einer rangeht."
			));
			$('#game').append(br);

			text_count = 2006.18;
			return;
		}

		if (e.which == 13 && text_count == 2006.18){
			$('#game').append(storyText(
				name_norbert,
				"Als wenn hier in 200000 Meter Tiefe, überhaupt jemand."
			));
			$('#game').append(br);

			text_count = 2006.19;
			return;
		}

		if (e.which == 13 && text_count == 2006.19){
			$('#game').append(storyOnlyText(
				"*Stimme im Telefon*"
			));
			$('#game').append(br);

			text_count = 2006.20;
			return;
		}

		if (e.which == 13 && text_count == 2006.20){
			$('#game').append(storyText(
				name_guenther,
				"Hallo und Willkommen zu Günther's Schlüsseldienst, leider sind wir momentan nicht erreichbar. Versuchen Sie es sonst später nochmal, hinterlassen Sie eine Nachricht nach dem Ton, oder geben Sie im Verlauf des Spieles die 2854 ein, um mich erneut anzurufen."
			));

			text_count = 2003;
			return;
		}
		// QUESTION 3 (SPEZIALFÄHIGKEIT 2) [end]

		// QUESTION 3 (GELBE VASE) [start]
		if (e.which == 13 && text_count == 2007.10){
			$('#game').append(storyOnlyText(
				"*Ein Spiral-Staubsauger-Roboter DES TODES!!! erscheint und tötet dich*"
			));
			$('#game').append(br);

			text_count = 2007.11;
			return;
		}

		if (e.which == 13 && text_count == 2007.11){
			$('#game').append(storyOnlyText(
				"GAME OVER BITCH !!!!"
			));

			text_count = 2003;
			return;
		}
		// QUESTION 3 (GELBE VASE) [end]

		// QUESTION 3 (STEIN UNTERSUCHEN) [start]
		if (e.which == 13 && text_count == 2008.10){
			$('#game').append(storyOnlyText(
				"Gebe <strong>2602</strong> ein um fortzufahren."
			));

			text_count = 2008.11;
			return;
		}

		if (e.which == 13 && text_count == 2008.11){
			var question_2008_11_2602_text = $('#text-field').val().toUpperCase();
			if(question_2008_11_2602_text.length == 4){
				if(question_2008_11_2602_text == "2602"){
					$('#game').empty();
					$('#text-field').val("");
					$('#game').append(storyOnlyText(
						"Die Tür wurde geöffnet und es scheint so, als wenn eine Dunkle Aura auf der anderen Seite der Tür lauern würde. Ungeachtet dessen - und weil du keine andere Möglichkeit hast - schreitet ihr hindurch."
					));
					$('#game').append(br);

					text_count = 3000;
				}
			}
			return;
		}
		// QUESTION 3 (STEIN UNTERSUCHEN) [end]

		// QUESTION 3 (TÜR ÖFFNEN) [start]
		if (e.which == 13 && text_count == 2006.10){
			$('#game').append(storyText(
				name_freia,
				"Jeap, ich möchte heute noch weiterkommen, damit ich sobald ich zuhause bin, endlich wieder mit meinem Haustier spielen kann ^^"
			));

			text_count = 2006.11;
			return;
		}

		if (e.which == 13 && text_count == 2006.11){
			$('#game').empty();
			$('#game').append(storyText(
				name_norbert,
				"Ohhhhhh. Lass mich raten, es ist ein .....",
				" ",
				"E - Elefant",
				"C - Chamäleon",
				"B - Baby Dinosaurier"
			));

			text_count = 2006.12;
			return;
		}

		if (e.which == 13 && text_count == 2006.12){
			var question_2006_12_text = $('#text-field').val().toUpperCase();
			if(question_2006_12_text.length >= 1){
				if(question_2006_12_text == "E" || question_2006_12_text == "ELEFANT"){
					$('#game').empty();
					$('#text-field').val("");
					$('#game').append(storyText(
						name_ich,
						"Elefant, denn er passt ja zu dir weil ...... Oh Fuck, lass mich raten O.O"
					));
					$('#game').append(br);

					text_count = 2006.1210;
					return;
				}
				if(question_2006_12_text == "C" || question_2006_12_text == "CHAMÄLEON"){
					$('#game').empty();
					$('#text-field').val("");
					$('#game').append(storyText(
						name_ich,
						"Ein .... Chamäleon?"
					));
					$('#game').append(br);

					text_count = 2006.1310;
					return;
				}
				if(question_2006_12_text == "B" || question_2006_12_text == "BABY DINOSAURIER"){
					$('#game').empty();
					$('#text-field').val("");
					$('#game').append(storyText(
						name_ich,
						"Einen kleinen süßen Baby Dinosaurier ^^"
					));
					$('#game').append(br);

					text_count = 2006.1410;
					return;
				}
			}else{
				text_count = 2006.11;
			}
			return;
		}
		// QUESTION 3.1 (ELEFANT) [start]
		if (e.which == 13 && text_count == 2006.1210){
			$('#game').append(storyText(
				name_freia,
				"Pushdug Saruman-glob búbhosh ska Uglúk u bagronk sha!!!!!!!!!"
			));
			$('#game').append(br);

			text_count = 2006.1211;
			return;
		}

		if (e.which == 13 && text_count == 2006.1211){
			$('#game').append(storyOnlyText(
				"GAME OVER BITCH"
			));

			text_count = 2006.11;
			return;
		}
		// QUESTION 3.1 (ELEFANT) [end]

		// QUESTION 3.1 (CHAMÄLEON) [start]
		if (e.which == 13 && text_count == 2006.1310){
			$('#game').append(storyText(
				name_freia,
				"Jeap, das ist komplett richtig ^^. Und sobald ich halt mit unserem neuen Freund hier das Game geschafft habe, kann ich endlich wieder zurück zu Miss Elli *freu*"
			));

			text_count = 2006.15;
			return;
		}
		// QUESTION 3.1 (CHAMÄLEON) [end]

		// QUESTION 3.1 (BABY DINOSAURIER) [start]
		if (e.which == 13 && text_count == 2006.1410){
			$('#game').append(storyText(
				name_freia,
				"Ähmmm nein, ich hab ein Chamäleon. Die Dinosaurier verschwanden vor rund 65 Millionen Jahren von der Erde. Nach 160 Millionen Jahren, starben sie am Ende der Kreidezeit aus. Auch während des Erdmittelalters gab es Dinosaurier, die sich entwickelten, ausstarben oder weiterentwickelten. So starben zum Beispiel die Stacheldinosaurier schon 60 Millionen Jahre vor den letzten Dinosaurierarten aus. Aber schön, dass wir darüber geredet haben."
			));

			text_count = 2006.15;
			return;
		}
		// QUESTION 3.1 (BABY DINOSAURIER) [end]

		if (e.which == 13 && text_count == 2006.15){
			$('#game').empty();
			$('#game').append(storyQuestion(
				"Was tust du?",
				"G - Gelbe Vase durchsuchen",
				"S - Den Stein untersuchen"
			));

			text_count = 2006.16;
			return;
		}

		if (e.which == 13 && text_count == 2006.16){
			var question_2006_16_text = $('#text-field').val().toUpperCase();
			if(question_2006_16_text.length >= 1){
				if(question_2006_16_text == "G" || question_2006_16_text == "GELBE VASE DURCHSUCHEN"){
					$('#game').empty();
					$('#text-field').val("");
					$('#game').append(storyText(
						name_norbert,
						"Na dann lass uns mal schauen, was du da schönes ...."
					));
					$('#game').append(br);

					text_count = 2006.1610;
					return;
				}
				if(question_2006_16_text == "S" || question_2006_16_text == "DEN STEIN UNTERSUCHEN"){
					$('#game').empty();
					$('#text-field').val("");
					$('#game').append(storyText(
						name_norbert,
						"Du untersuchst den Stein und findest einen Schlüssel mit einer 2602 eingraviert."
					));
					$('#game').append(br);

					text_count = 2006.1710;
					return;
				}
			}else{
				text_count = 2006.15;
			}
			return;
		}

		// QUESTION 3.2 (GELBE VASE) [start]
		if (e.which == 13 && text_count == 2006.1610){
			$('#game').append(storyOnlyText(
				"*Ein Spiral-Staubsauger-Roboter DES TODES!!! erscheint und tötet dich*"
			));
			$('#game').append(br);

			text_count = 2006.1611;
			return;
		}

		if (e.which == 13 && text_count == 2006.1611){
			$('#game').append(storyOnlyText(
				"GAME OVER BITCH !!!!"
			));

			text_count = 2006.15;
			return;
		}
		// QUESTION 3.2 (GELBE VASE) [end]

		// QUESTION 3.2 (STEIN UNTERSUCHEN) [start]
		if (e.which == 13 && text_count == 2006.1710){
			$('#game').append(storyOnlyText(
				"Gebe <strong>2602</strong> ein um fortzufahren."
			));

			text_count = 2006.1711;
			return;
		}

		if (e.which == 13 && text_count == 2006.1711){
			var question_206_1711_2602_text = $('#text-field').val().toUpperCase();
			if(question_206_1711_2602_text.length == 4){
				if(question_206_1711_2602_text == "2602"){
					$('#game').empty();
					$('#text-field').val("");
					$('#game').append(storyOnlyText(
						"Die Tür wurde geöffnet und es scheint so, als wenn eine Dunkle Aura auf der anderen Seite der Tür lauern würde. Ungeachtet dessen - und weil du keine andere Möglichkeit hast - schreitet ihr hindurch."
					));
					$('#game').append(br);

					text_count = 3000;
				}
			}
			return;
		}
		// QUESTION 3.2 (STEIN UNTERSUCHEN) [end]

		// QUESTION 3 (TÜR ÖFFNEN) [end]
		// CHAPTER 2 [end]
	});
});