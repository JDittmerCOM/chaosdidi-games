$(document).ready(function(){
	$('#text-field').keypress(function(e){
		$('html, body').animate({scrollTop: $(document).height()}, 50);

		// CHAPTER 3 [start]
		if (e.which == 13 && text_count == 3000){
			$('#current_chapter').empty();
			$('#current_chapter').append(chapter("Kapitel 3: Realität über Logik"));
			
			$('#game').append(storyText(
				name_norbert,
				"Nachdem du durch die Tür gegangen bist, findest du dich in einem dunklen und gruseligem Wald wieder. Die Geister der verdorbenen Krieger des Todes umklammern eisern die kalte Luft der Morgensonne, während ihr euch auf eine Verschlossene Tür zubewegt, welche ummantelt mit Polypropylen ist."
			));
			$('#game').append(br);

			text_count = 3001;
			return;
		}

		if (e.which == 13 && text_count == 3001){
			$('#game').append(storyText(
				name_freia,
				"Oh Man, können wir nicht einfach uns in einer anderen Szene befinden? Ich mag das hier nicht. Mir ist kalt, es ist zu dunkel und deine Hand auf meiner Schulter fühlt sich kalt und Tot an *Bibber*"
			));
			$('#game').append(br);

			text_count = 3002;
			return;
		}

		if (e.which == 13 && text_count == 3002){
			$('#game').append(storyText(
				name_norbert,
				"1. Nein, wir können die Szene nicht SCHON WIEDER ändern. In einer zuckersüßen Welt, voll mit Einhörnern und Glücksbärchis, würde sich wohl kaum einer fürchten!!!!!!!!!.",
				"2. Wenn dir kalt ist, warum benutzt du nicht deine Magie und wärmst dich auf?!",
				"Und 3. hab ich dir doch schon erklärt, dass ich nur im Geiste bei dir bin. Wie soll ich dann meine Hand auf deiner Schulter haben?"
			));
			$('#game').append(br);

			text_count = 3003;
			return;
		}

		if (e.which == 13 && text_count == 3003){
			$('#game').append(storyText(
				name_freia,
				"Ach ja, stimmt ja ich bin ja eine Magierin *Die Hand auf die Stirn klatsch*"
			));
			$('#game').append(br);

			text_count = 3004;
			return;
		}

		if (e.which == 13 && text_count == 3004){
			$('#game').append(storyText(
				name_freia,
				"Aber wenn das nicht deine Hand ist ....."
			));
			$('#game').append(br);

			text_count = 3005;
			return;
		}

		if (e.which == 13 && text_count == 3005){
			$('#game').append(storyText(
				name_freia,
				"WAHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH!!!!!!!!!!!!"
			));
			$('#game').append(br);

			text_count = 3006;
			return;
		}

		if (e.which == 13 && text_count == 3006){
			$('#game').empty();
			$('#game').append(storyQuestion(
				"Freia ist in Gefahr und braucht deine Hilfe. Welchen Zauber soll Sie benutzen?",
				"F - Feuerball",
				"W - Wassermagie",
				"L - Lichtzauber"
			));
			$('#game').append(br);

			text_count = 3007;
			return;
		}

		if (e.which == 13 && text_count == 3007){
			var question_3007_text = $('#text-field').val().toUpperCase();
			if(question_3007_text.length >= 1){
				if(question_3007_text == "F" || question_3007_text == "FEUERBALL"){
					$('#game').empty();
					$('#text-field').val("");
					$('#game').append(storyText(
						name_ich,
						"Feuerball. Schnell, benutze deinen Feuerball!!!"
					));
					$('#game').append(br);

					text_count = 3008.10;
					return;
				}
				if(question_3007_text == "W" || question_3007_text == "WASSERMAGIE"){
					$('#game').empty();
					$('#text-field').val("");
					$('#game').append(storyText(
						name_ich,
						"Wassermagie. Schnell, benutze deine Wassermagie!!!"
					));
					$('#game').append(br);

					text_count = 3009.10;
					return;
				}
				if(question_3007_text == "L" || question_3007_text == "LICHTZAUBER"){
					$('#game').empty();
					$('#text-field').val("");
					$('#game').append(storyText(
						name_ich,
						"Lichtzauber. Schnell, benutze deinen Lichtzauber!!!"
					));
					$('#game').append(br);

					text_count = 3010.10;
					return;
				}
			}else{
				text_count = 3006;
			}
			return;
		}

		// QUESTION 4 (FEUERBALL) [start]
		if (e.which == 13 && text_count == 3008.10){
			$('#game').append(storyText(
				name_freia,
				"Ja okay."
			));
			$('#game').append(br);

			text_count = 3008.11;
			return;
		}

		if (e.which == 13 && text_count == 3008.11){
			$('#game').append(storyText(
				name_freia,
				"ASKION KATASKION LIX TETRAX DAMNAMENEUS AISION"
			));
			$('#game').append(br);

			text_count = 3008.12;
			return;
		}

		if (e.which == 13 && text_count == 3008.12){
			$('#game').append(storyOnlyText(
				"*Freia benutzt den Feuerball aber er trifft ins Leere*"
			));
			$('#game').append(br);

			text_count = 3008.13;
			return;
		}

		if (e.which == 13 && text_count == 3008.13){
			$('#game').empty();
			$('#game').append(storyText(
				name_freia,
				"Und was jetzt?",
				" ",
				"N - Nochmal benutzen",
				"1 - Spezialfähigkeit 1 benutzen (Kommunikation)"
			));
			$('#game').append(br);

			text_count = 3008.14;
			return;
		}

		if (e.which == 13 && text_count == 3008.14){
			var question_3008_14_text = $('#text-field').val().toUpperCase();
			if(question_3008_14_text.length >= 1){
				if(question_3008_14_text == "N" || question_3008_14_text == "NOCHMAL BENUTZEN"){
					$('#game').empty();
					$('#text-field').val("");
					$('#game').append(storyText(
						name_ich,
						"Benutze den gleichen Zauber, der gerade ebend schon keine Wirkung einfach hatte einfach noch einmal. Vielleicht klappt es ja jetzt O.o"
					));
					$('#game').append(br);

					text_count = 3008.1510;
					return;
				}
				if(question_3008_14_text == "1" || question_3008_14_text == "SPEZIALFÄHIGKEIT 1 BENUTZEN"){
					$('#game').empty();
					$('#text-field').val("");
					$('#game').append(storyText(
						name_ich,
						"Spezialfähigkeit 1 benutzen"
					));
					$('#game').append(br);

					text_count = 3008.1610;
					return;
				}
			}else{
				text_count = 3008.13;
			}
			return;
		}
		// QUESTION 4.1 (NOCHMAL BENUTZEN) [start]
		if (e.which == 13 && text_count == 3008.1510){
			$('#game').append(storyText(
				name_freia,
				"Also ich glaube, du hast die Logik bereits selbst festgestellt, als du das eingegeben hast oder?!?!?!"
			));
			$('#game').append(br);

			text_count = 3008.1511;
			return;
		}

		if (e.which == 13 && text_count == 3008.1511){
			$('#game').append(storyText(
				name_freia,
				"ASKION KATASKION LIX TETRAX DAMNAMENEUS AISION"
			));
			$('#game').append(br);

			text_count = 3008.1512;
			return;
		}

		if (e.which == 13 && text_count == 3008.1512){
			$('#game').append(storyOnlyText(
				"*Du benutzt den Feuerball aber er trifft ins Leere*"
			));
			$('#game').append(br);

			text_count = 3008.1513;
			return;
		}

		if (e.which == 13 && text_count == 3008.1513){
			$('#game').append(storyText(
				name_geist,
				"Hiaus. Mga tawo nga tinuod nga awkward panahon O.Ö. Okay, nan mahanaw sa mga panahon. sa dcnno"
			));
			$('#game').append(br);

			text_count = 3008.1514;
			return;
		}

		if (e.which == 13 && text_count == 3008.1514){
			$('#game').append(storyText(
				name_norbert,
				"Der Geist verschwindet und während er sich auflöst, siehst du das er einen Schlüssel bei sich hatte *Pech gehabt* Kannst du jemand bestimmten anrufen der dir vielleicht die Tür öffnet, ansonsten heißt es:",
				" ",
				"GAME OVER BITCH!!!!"
			));
			$('#game').append(br);

			text_count = 3008.1515;
			return;
		}

		if (e.which == 13 && text_count == 3008.1515){
			var question_3008_1515_2854_text = $('#text-field').val().toUpperCase();
			if(question_3008_1515_2854_text.length == 4){
				if(question_3008_1515_2854_text == "2854"){
					$('#game').empty();
					$('#text-field').val("");
					$('#game').append(storyText(
						name_freia,
						"Es klingelt. Jetzt hoffen wir mal dass diesmal auch jem..."
					));
					$('#game').append(br);

					text_count = 3011.10;
				}
			}else{
				text_count = 3006;
			}
			return;
		}
		// QUESTION 4.1 (NOCHMAL BENUTZEN) [end]

		// QUESTION 4.2 (SPEZIALFÄHIGKEIT 1) [start]
		if (e.which == 13 && text_count == 3008.1610){
			$('#game').append(storyText(
				name_freia,
				"Hallo Herr Geist. Wie geht es ihnen denn so? ^^"
			));
			$('#game').append(br);

			text_count = 3008.1611;
			return;
		}

		if (e.which == 13 && text_count == 3008.1611){
			$('#game').append(storyText(
				name_geist,
				"Och ja Dankeschön. Muss ja ^^. Und selbst so? Was sollte denn gerade dieser fehlgeschlagende Angriff auf mich deuten? Es scheint fast so, als wenn eine Art Kommunikationsschwankung zwischen uns herrscht."
			));
			$('#game').append(br);

			text_count = 3008.1612;
			return;
		}

		if (e.which == 13 && text_count == 3008.1612){
			$('#game').append(storyText(
				name_freia,
				"Ich wollte Sie ja eigentlich nicht angreifen, aber unser Held hier hat dummerweise sich zum angreifen entschieden *Seufz*"
			));
			$('#game').append(br);

			text_count = 3008.1613;
			return;
		}

		if (e.which == 13 && text_count == 3008.1613){
			$('#game').append(storyText(
				name_geist,
				"Ach jetzt erinnere ich mich auch wieder an Sie O.o"
			));
			$('#game').append(br);

			text_count = 3008.1614;
			return;
		}

		if (e.which == 13 && text_count == 3008.1614){
			$('#game').append(storyText(
				name_geist,
				"Waren Sie nicht die Dame, die letztens mit Miss Ellie in Edats einkaufen ging? Wie geht es ihrem Chamäleon?"
			));
			$('#game').append(br);

			text_count = 3008.1615;
			return;
		}

		if (e.which == 13 && text_count == 3008.1615){
			$('#game').append(storyText(
				name_freia,
				"Och Danke der Nachfrage ^^. Also sobald ich hier durch bin, kann ich wieder zu Miss Ellie. Solange ich nicht durch diese Polypropylen Tür komme, wird mein süßes Tierchen wohl verhungern :("
			));
			$('#game').append(br);

			text_count = 3008.1616;
			return;
		}

		if (e.which == 13 && text_count == 3008.1616){
			$('#game').append(storyText(
				name_geist,
				"Wenn man davon mal absieht, dass Polypropylen der Fachbegriff für Plastik ist, warum hast du das nicht gleich gesagt?"
			));
			$('#game').append(br);

			text_count = 3008.1617;
			return;
		}

		if (e.which == 13 && text_count == 3008.1617){
			$('#game').append(storyText(
				name_norbert,
				"Der Geist sucht in seiner ........ Keine Ahnung was das ist. Das ist eine Art Tasche, aber irgendwie sind da Knochen und menschliche Überreste drinne *Würg*"
			));
			$('#game').append(br);

			text_count = 3008.1618;
			return;
		}

		if (e.which == 13 && text_count == 3008.1618){
			$('#game').append(storyText(
				name_geist,
				"Wollt ihr nun den Schlüssel haben oder nicht? `.´"
			));
			$('#game').append(br);

			text_count = 3008.1619;
			return;
		}

		if (e.which == 13 && text_count == 3008.1619){
			$('#game').append(storyText(
				name_freia,
				"Ja, das wäre ganz lieb von dir ^^"
			));
			$('#game').append(br);

			text_count = 3008.1620;
			return;
		}

		if (e.which == 13 && text_count == 3008.1620){
			$('#game').append(storyText(
				name_geist,
				"Na Klaro doch. Hauptsache du kommst schnell zu Ellie zurück. Grüß Sie schön von mir ^^"
			));
			$('#game').append(br);

			text_count = 3008.1621;
			return;
		}

		if (e.which == 13 && text_count == 3008.1621){
			$('#game').append(storyText(
				name_freia,
				"Vielen Dank nochmal und nen schönen Tag noch ^^"
			));
			$('#game').append(br);

			text_count = 3008.1622;
			return;
		}

		if (e.which == 13 && text_count == 3008.1622){
			$('#game').append(storyText(
				name_norbert,
				"Wow, das hab ich so nicht kommen sehen O.o"
			));
			$('#game').append(br);

			text_count = 3008.1623;
			return;
		}

		if (e.which == 13 && text_count == 3008.1623){
			$('#game').append(storyText(
				name_norbert,
				"Nachdem die Sonne aufgegangen ist, gehen Du und Freia durch die Tür und schließen diese hinter sich."
			));
			$('#game').append(br);

			text_count = 4000;
			return;
		}
		// QUESTION 4.2 (SPEZIALFÄHIGKEIT 1) [end]
		// QUESTION 4 (FEUERBALL) [end]

		// QUESTION 4 (WASSERMAGIE) [start]
		if (e.which == 13 && text_count == 3009.10){
			$('#game').append(storyText(
				name_freia,
				"Ja okay. Du bist der Chef, nur beantworte mir bitte eine klitzekleine Frage ^^"
			));
			$('#game').append(br);

			text_count = 3009.11;
			return;
		}

		if (e.which == 13 && text_count == 3009.11){
			$('#game').append(storyText(
				name_freia,
				"WAS SOLL IN DIESER SITUATION WASSERMAGIE ANRICHTEN?!?!?!?! SOLL DER GEIST EINE TÖDLICHE LUNGENENTZÜNDUNG BEKOMMEN ODER WILLST DU IHM EINE WOHLTUENDE MASSAGE ANBIETEN?!?!? VIELLEICHT EIN GLAS WASSER HERR GEIST?!?!?"
			));
			$('#game').append(br);

			text_count = 3009.12;
			return;
		}

		if (e.which == 13 && text_count == 3009.12){
			$('#game').append(storyText(
				name_freia,
				"Hoc est corpus meum"
			));
			$('#game').append(br);

			text_count = 3009.13;
			return;
		}

		if (e.which == 13 && text_count == 3009.13){
			$('#game').append(storyOnlyText(
				"*Es zeigt keine Wirkung*"
			));
			$('#game').append(br);

			text_count = 3009.14;
			return;
		}

		if (e.which == 13 && text_count == 3009.14){
			$('#game').append(storyText(
				name_freia,
				"Ach ne, das hätte ich nicht gedacht!!!!",
				"Das konnte ja keiner ahnen *Hand auf die Stirn klatsch*"
			));
			$('#game').append(br);

			text_count = 3009.15;
			return;
		}

		if (e.which == 13 && text_count == 3009.15){
			$('#game').append(storyText(
				name_sternchen,
				"Hey Leute, ich mache auch nur meinen Job und erkläre die Situation ...."
			));
			$('#game').append(br);

			text_count = 3009.16;
			return;
		}

		if (e.which == 13 && text_count == 3009.16){
			$('#game').append(storyText(
				name_freia,
				"Ja, du bist ja auch nicht schuld, sondern der Depp, der meint das jetzt gerade Wassermagie hilft -.-"
			));
			$('#game').append(br);

			text_count = 3009.17;
			return;
		}

		if (e.which == 13 && text_count == 3009.17){
			$('#game').append(storyText(
				name_geist,
				"Hiaus. Mga tawo nga tinuod nga awkward panahon O.Ö. Okay, nan mahanaw sa mga panahon. sa dcnno"
			));
			$('#game').append(br);

			text_count = 3009.18;
			return;
		}

		if (e.which == 13 && text_count == 3009.18){
			$('#game').append(storyText(
				name_norbert,
				"Der Geist verschwindet und während er sich auflöst, siehst du das er einen Schlüssel bei sich hatte *Pech gehabt* Kannst du jemand bestimmten anrufen der dir vielleicht die Tür öffnet, ansonsten heißt es:",
				" ",
				"GAME OVER BITCH!!!!"
			));
			$('#game').append(br);

			text_count = 3009.19;
			return;
		}

		if (e.which == 13 && text_count == 3009.19){
			var question_3009_19_2854_text = $('#text-field').val().toUpperCase();
			if(question_3009_19_2854_text.length == 4){
				if(question_3009_19_2854_text == "2854"){
					$('#game').empty();
					$('#text-field').val("");
					$('#game').append(storyText(
						name_freia,
						"Es klingelt. Jetzt hoffen wir mal dass diesmal auch jem..."
					));
					$('#game').append(br);

					text_count = 3011.10;
				}
			}else{
				text_count = 3006;
			}
			return;
		}
		// QUESTION 4 (WASSERMAGIE) [end]

		// QUESTION 4 (LICHTZAUBER) [start]
		if (e.which == 13 && text_count == 3010.10){
			$('#game').append(storyText(
				name_freia,
				"Ja okay."
			));
			$('#game').append(br);

			text_count = 3010.11;
			return;
		}

		if (e.which == 13 && text_count == 3010.11){
			$('#game').append(storyText(
				name_freia,
				"SATOR AREPO TENET OPERA ROTAS"
			));
			$('#game').append(br);

			text_count = 3010.12;
			return;
		}

		if (e.which == 13 && text_count == 3010.12){
			$('#game').append(storyOnlyText(
				"*Der Lichtzauber löst den Geist in ..... ja kann man nichts sagen? Weil eigentlich besteht so ein Geist ja aus nichts. Also von daher kann sich ein ......*"
			));
			$('#game').append(br);

			text_count = 3010.13;
			return;
		}

		if (e.which == 13 && text_count == 3010.13){
			$('#game').append(storyText(
				name_freia,
				"Soll ein * nicht eigentlich nur eine Aktion darstellen und nicht gleich eine Logik oder eine komplette Analyse beinhalten? Ö.O"
			));
			$('#game').append(br);

			text_count = 3010.14;
			return;
		}

		if (e.which == 13 && text_count == 3010.14){
			$('#game').append(storyText(
				name_sternchen,
				"Okay okay okay. Dann halt Kurzversion!!!",
				"Der Geist ist weg, er hat nen Schlüssel fallen gelassen und ihr könnt mich mal :P !!!!!"
			));
			$('#game').append(br);

			text_count = 3010.15;
			return;
		}

		if (e.which == 13 && text_count == 3010.15){
			$('#game').append(storyText(
				name_freia,
				"Wow, das hab ich so nicht kommen sehen O.o"
			));
			$('#game').append(br);

			text_count = 3010.16;
			return;
		}

		if (e.which == 13 && text_count == 3010.16){
			$('#game').append(storyText(
				name_norbert,
				"Nachdem die Sonne aufgegangen ist, gehen Du und Freia durch die Tür und schließen diese hinter sich."
			));
			$('#game').append(br);

			text_count = 4000;
			return;
		}
		// QUESTION 4 (LICHTZAUBER) [end]

		// QUESTION 4 (2854) [start]
		if (e.which == 13 && text_count == 3011.10){
			$('#game').append(storyText(
				name_guenther,
				"Willkommen bei Günthers Schlüsseldienst, was kann ich für Sie tun?"
			));
			$('#game').append(br);

			text_count = 3011.11;
			return;
		}

		if (e.which == 13 && text_count == 3011.11){
			$('#game').append(storyText(
				name_freia,
				"Halli Hallo Herr Günther, könnten Sie uns wohl freundlicherweise diese Tür öffnen?"
			));
			$('#game').append(br);

			text_count = 3011.12;
			return;
		}

		if (e.which == 13 && text_count == 3011.12){
			$('#game').append(storyText(
				name_guenther,
				"Schönen guten Tag Frau Freia. Ja, das werde ich wohl hinbekommen ^^"
			));
			$('#game').append(br);

			text_count = 3011.13;
			return;
		}

		if (e.which == 13 && text_count == 3011.13){
			$('#game').append(storyText(
				name_freia,
				"Woher kennen Sie meinen Namen?!"
			));
			$('#game').append(br);

			text_count = 3011.14;
			return;
		}

		if (e.which == 13 && text_count == 3011.14){
			$('#game').append(storyText(
				name_guenther,
				"Na das steht doch die Ganze Zeit schon im Chat drinne. Wer lesen kann, ist klar im Vorteil *Lach*"
			));
			$('#game').append(br);

			text_count = 3011.15;
			return;
		}

		if (e.which == 13 && text_count == 3011.15){
			$('#game').append(storyText(
				name_freia,
				"Oh."
			));
			$('#game').append(br);

			text_count = 3011.16;
			return;
		}

		if (e.which == 13 && text_count == 3011.16){
			$('#game').append(storyText(
				name_guenther,
				"Sie müssen mir nur ihre genauen Aufenhaltsdaten weitergeben, damit ich mich zu ihnen hinteleportieren kann."
			));
			$('#game').append(br);

			text_count = 3011.17;
			return;
		}

		if (e.which == 13 && text_count == 3011.17){
			$('#game').append(storyText(
				name_freia,
				"Okay. Also. Zuerst waren wir bei so einer Tür. Da war dann Links sone Vase und Rechts son Stein. Und unter dem Stein war dann son komischer Schlüssel, mit dem man die Tür aufmachen konnte. Und als wir dann die Tür aufgemacht haben, kamen wir in sonen Wald und da war dann son Wald und da war es voll gruselig und so. Und als wir dann so weitergingen, war ich voll erschrocken, weil da son perverser Geist einfach seine Hand auf meine Schulter gelegt hat. Und dann hab ich meine Magier benutzt und dann hat das nicht geklappt. Und dann ist der Geist weggeflogen und dann war ich so halt voll in Panik und hab Sie einfach angerufen."
			));
			$('#game').append(br);

			text_count = 3011.18;
			return;
		}

		if (e.which == 13 && text_count == 3011.18){
			$('#game').append(storyText(
				name_guenther,
				"........"
			));
			$('#game').append(br);

			text_count = 3011.19;
			return;
		}

		if (e.which == 13 && text_count == 3011.19){
			$('#game').append(storyText(
				name_guenther,
				"Ist bei ihnen noch jemand, der mir eine ETWAS GENAUERE Position geben kann?! Wissen Sie eigentlich, wieviele Dungeon's genau dieses Szenario beinhalten, dass Sie mir gerade genannt haben?!?!?!?!?!?!?!?"
			));
			$('#game').append(br);

			text_count = 3011.20;
			return;
		}

		if (e.which == 13 && text_count == 3011.20){
			$('#game').append(storyText(
				name_freia,
				"Keine Ahnung, vielleicht so um die 2-3 Stück? *unschuldig guck*"
			));
			$('#game').append(br);

			text_count = 3011.21;
			return;
		}

		if (e.which == 13 && text_count == 3011.21){
			$('#game').append(storyText(
				name_guenther,
				"Es gibt exakt 24865 Stück O.o"
			));
			$('#game').append(br);

			text_count = 3011.22;
			return;
		}

		if (e.which == 13 && text_count == 3011.22){
			$('#game').append(storyText(
				name_norbert,
				"Wir sind auf Position 9.01.1969."
			));
			$('#game').append(br);

			text_count = 3011.23;
			return;
		}

		if (e.which == 13 && text_count == 3011.23){
			$('#game').append(storyText(
				name_guenther,
				"Ahhhhhh, na sagen Sie das doch gleich."
			));
			$('#game').append(br);

			text_count = 3011.24;
			return;
		}

		if (e.which == 13 && text_count == 3011.24){
			$('#game').append(storyOnlyText(
				"*Plop*"
			));
			$('#game').append(br);

			text_count = 3011.25;
			return;
		}

		if (e.which == 13 && text_count == 3011.25){
			$('#game').append(storyText(
				name_freia,
				"Wow, das ging ja mal wirklich schnell O.o. Sie werden bestimmt nicht nach Zeit bezahlt *Lach*"
			));
			$('#game').append(br);

			text_count = 3011.26;
			return;
		}

		if (e.which == 13 && text_count == 3011.26){
			$('#game').append(storyText(
				name_guenther,
				"Neeyyy. Das wüsste ich doch. Ich werde pro Auftrag bezahlt."
			));
			$('#game').append(br);

			text_count = 3011.27;
			return;
		}

		if (e.which == 13 && text_count == 3011.27){
			$('#game').append(storyText(
				name_guenther,
				"Wo liegt denn jetzt das Problem? Und dieses Mal bitte die KURZVERSION -.-"
			));
			$('#game').append(br);

			text_count = 3011.28;
			return;
		}

		if (e.which == 13 && text_count == 3011.28){
			$('#game').append(storyOnlyText(
				"*Freia holt Luft*"
			));
			$('#game').append(br);

			text_count = 3011.29;
			return;
		}

		if (e.which == 13 && text_count == 3011.29){
			$('#game').append(storyText(
				name_geist,
				"Weg. Schlüssel. Weg."
			));
			$('#game').append(br);

			text_count = 3011.30;
			return;
		}

		if (e.which == 13 && text_count == 3011.30){
			$('#game').append(storyText(
				name_guenther,
				"Na das ist doch mal eine knappe und bündige Aussage O.o"
			));
			$('#game').append(br);

			text_count = 3011.31;
			return;
		}

		if (e.which == 13 && text_count == 3011.31){
			$('#game').append(storyText(
				name_norbert,
				"Günther schaut sich die Tür an und nimmt eine Probe des Polypropylen Stoffes."
			));
			$('#game').append(br);

			text_count = 3011.32;
			return;
		}

		if (e.which == 13 && text_count == 3011.32){
			$('#game').append(storyText(
				name_guenther,
				"Muss der alles was ich mache kommentieren? Ö.ö"
			));
			$('#game').append(br);

			text_count = 3011.33;
			return;
		}

		if (e.which == 13 && text_count == 3011.33){
			$('#game').append(storyText(
				name_freia,
				"Das ist leider seine Aufgabe *Seufz*"
			));
			$('#game').append(br);

			text_count = 3011.34;
			return;
		}

		if (e.which == 13 && text_count == 3011.34){
			$('#game').append(storyText(
				name_guenther,
				"Okay. Ich mache also den weiten Weg bis hier hin, überquere dutzende Todesfallen, während mich ...."
			));
			$('#game').append(br);

			text_count = 3011.35;
			return;
		}

		if (e.which == 13 && text_count == 3011.35){
			$('#game').append(storyText(
				name_freia,
				"Sie haben sich doch hier hin teleportiert. Sie mussten doch gar keine Fallen überqueren Ö.ö"
			));
			$('#game').append(br);

			text_count = 3011.36;
			return;
		}

		if (e.which == 13 && text_count == 3011.36){
			$('#game').append(storyText(
				name_guenther,
				"Worauf ich hinaus will. Wissen Sie was eigentlich was Polypropylen ist?!?!?!?!?!?!?!?!?"
			));
			$('#game').append(br);

			text_count = 3011.37;
			return;
		}

		if (e.which == 13 && text_count == 3011.37){
			$('#game').append(storyText(
				name_freia,
				"Also das klingt schon mal richtig Chemikalisch und voll Gefährlich."
			));
			$('#game').append(br);

			text_count = 3011.38;
			return;
		}

		if (e.which == 13 && text_count == 3011.38){
			$('#game').append(storyText(
				name_guenther,
				"Plastik. Polypropylen ist einfach nur der Fachbegriff für PLASTIK!!!!!"
			));
			$('#game').append(br);

			text_count = 3011.39;
			return;
		}

		if (e.which == 13 && text_count == 3011.39){
			$('#game').append(storyText(
				name_guenther,
				"Diese Tür besteht einfach nur aus Plastik ..... Sie hätten diese selbst mit einem Stein öffnen können -.-"
			));
			$('#game').append(br);

			text_count = 3011.40;
			return;
		}

		if (e.which == 13 && text_count == 3011.40){
			$('#game').append(storyText(
				name_guenther,
				"Ich schick ihnen die Rechnung dann zu. Schönen Tag noch!!!"
			));
			$('#game').append(br);

			text_count = 3011.41;
			return;
		}

		if (e.which == 13 && text_count == 3011.41){
			$('#game').append(storyText(
				name_freia,
				"Wow, das hab ich so nicht kommen sehen O.o"
			));
			$('#game').append(br);

			text_count = 3011.42;
			return;
		}

		if (e.which == 13 && text_count == 3011.42){
			$('#game').append(storyText(
				name_norbert,
				"Nachdem die Sonne aufgegangen ist, gehen Du und Freia durch die Tür und schließen diese hinter sich."
			));
			$('#game').append(br);

			text_count = 4000;
			return;
		}
		// QUESTION 4 (2854) [end]
		// CHAPTER 3 [end]
	});
});