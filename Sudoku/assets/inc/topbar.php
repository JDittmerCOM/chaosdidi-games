<header class="navbar navbar-default navbar-fixed-top">
	<div class="container">
		<div class="navbar-header">
			<a href="./" class="navbar-brand visible-xs">ChaosDidi</a>
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"><i class="fa fa-bars"></i></button>
		</div>
		<div class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
				<li><a href="./">Startseite</a></li>
				<?
					if(ONLINE != '1'){
						echo "<li class='visible-xs'><a href='Login/Register'>Registrieren</a></li>";
						echo "<li class='visible-xs'><a href='Login/Login'>Einloggen</a></li>";
					}else{
						echo "<li class='visible-xs dropdown'>";
							echo "<a href='#' class='dropdown-toggle' data-toggle='dropdown'> ".USERNAME." <strong class='caret'></strong></a>";
						echo "</li>";
						echo "<li class='visible-xs'><a href='Login/Logout'>Ausloggen</a></li>";
					}
				?>
			</ul>
			<div class="pull-right navbar-buttons hidden-xs">
				<?
					if(ONLINE != '1'){
						echo "<a href='Login/Register' class='btn btn-primary'>Registrieren</a>";
						echo "<a href='Login/Login' class='btn btn-inverse'>Einloggen</a>";
					}else{
						echo "<div class='btn-group'>";
							echo "<button type='button' class='btn btn-primary dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>";
								echo USERNAME." <span class='caret'></span>";
							echo "</button>";
							echo "<ul class='dropdown-menu'>";
								echo "<li class='dropdown-header'>Kontostand: ".CREDITS." Pkt.</li>";
								echo "<li><a href='Balance/Overview'>Übersicht</a></li>";
								echo "<li><a href='Balance/AccountStatements'>Kontoauszüge</a></li>";
								echo "<li><a href='Balance/Donations'>Spenden</a></li>";
							echo "</ul>";
						echo "</div>";
						echo "<a href='Login/Logout' class='btn btn-inverse'>Ausloggen</a>";
					}
				?>
			</div>
		</div>
	</div>
</header>