/*
	#IchLiebeRestarts		Man fängt komplett von vorne an.
	#PorteMichZu[NUMBER]	Der Text-Counter auf eine beliebige Zahl gestellt wird.
	#ZeigeEntwickler		Credits werden aufgeführt.
*/

$(document).ready(function(){
	$('#text-field').keypress(function(e){
		$('html, body').animate({scrollTop: $(document).height()}, 50);

		// #IchLiebeRestarts [start]
		if (e.which == 13){
			var hash_IchLiebeRestarts = $('#text-field').val();
			if(hash_IchLiebeRestarts.length >= 1){
				if(hash_IchLiebeRestarts == "#IchLiebeRestarts"){
					$('#text-field').val("");

					text_count = 0;
					return;
				}
			}
		}
		// #IchLiebeRestarts [end]

		// #ZeigeEntwickler [start]
		if (e.which == 13){
			var hash_ZeigeEntwickler = $('#text-field').val();
			if(hash_ZeigeEntwickler.length >= 1){
				if(hash_ZeigeEntwickler == "#ZeigeEntwickler"){
					var OLD_text_count = text_count - 1;
					if(OLD_text_count > 0 && OLD_text_count < 1000){OLD_text_count = 0;}

					$('#game').empty();
					$('#text-field').val("");
					$('#current_chapter').empty();
					$('#current_chapter').append(chapter("Credits"));

					$('#game').append(storyOnlyText(
						nb(12)+"Story:"+nb(3)+"Kevin (ChaosDidi)",
						nb(3)+"Programmierung:"+nb(3)+"Justin (JND_3004)",
						" ",
						"TypeWriter Plugin:"+nb(3)+"Darcy Clarke"
					));

					text_count = OLD_text_count;
					return;
				}
			}
		}
		// #ZeigeEntwickler [end]
	});
});